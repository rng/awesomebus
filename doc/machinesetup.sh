#!/bin/bash

XMOS_TARBALL=~/Downloads/Desktop-Tools-\(Linux-32\)\(11.11.0\).tgz

echo "Installing development tools via apt-get"
sudo apt-get install junit4 openjdk-6-jdk mercurial eagle ant

echo "Cloning Awesomebus repository"
hg clone https://bitbucket.org/rng/awesomebus

cd awesomebus/
mkdir -p tools
cd tools

echo "Cloning Pindakaas repository"
hg clone https://bitbucket.org/rng/pindakaas

echo "Extracting XMOS tools"
tar -xzf $XMOS_TARBALL

cd ..

echo "Setting up setenv.sh script"
cp ./doc/setenv.sh-example ./setenv.sh

echo ""

