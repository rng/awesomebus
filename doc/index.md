% Awesomebus
% Ross Glashan, John Rebula
% June 10, 2012

<div id="content">

## Introduction

Awesomebus[^1] is a high-speed realtime network for controlling robot actuators. It was designed as an '[Ethercat][ec]-lite'. It features:

* Standard 100Mb Ethernet physical layer
* Up to 25 nodes per bus
* 1KHz+ control loop
* 5us per-node latency

[ec]: http://www.ethercat.org

[^1]: _BetterThanCANForOurUseCaseBus_ doesn't have the same ring to it.


## System

An Awesomebus network is composed of a single host device connected to a number of nodes via daisy-chained Ethernet cables (a node has 2 Ethernet ports). Each node drives one or more actuators.

<!-- ![Awesomebus System Diagram](system.png) -->

The host device is typically an embedded PC with onboard Ethernet. Our standard setup is based on the [Beaglebone][bb], but we also test on normal X86 Linux PCs.

Sitting between the host and the first node is a Power-over-Ethernet (PoE) injector, which is used to power the logic on the nodes. High current actuator supplies are wired separately.

[bb]: http://beagleboard.org/bone


## Bus Protocol

See the separate [protocol page][proto].

[proto]: ab-protocol.html

## Node Hardware

Each node consists of two or more boards. The _node board_ contains the hardware necessary to interface to the Awesomebus network, while the _driver board_ provides the hardware to drive and interface with any actuator hardware.

### Node Board

The node board consists of 3 main subsystems:

* Power regulation - 24V PoE to 3.3V and 1.0V regulation
* Awesomebus interface - Ethernet connectors and PHY connected to an Awesomebus MAC implement in an [XMOS][xmos]
* Node-Driver bridge - an ARM microcontroller which handles communication between the Awesomebus MAC and the driver board.

![Awesomebus Node Board](static/awesomebus_node.jpg)

[xmos]: http://www.xmos.com

### Driver Board

The driver board varies depending on the specific actuator hardware being driven. Currently we have a generic breakout board with an ARM microcontroller to experiment with interfacing to various actuator/sensor hardware.


## Node Firmware

The node firmware provides a basic register-based interface to the node and driver board functions.

The node is programmed in [Pindakaas][pk], which provides an interactive prompt for rapid development live on the node (via JTAG on the M3 CPU).

[pk]: https://bitbucket.org/rng/pindakaas


## Host Software

Since Awesomebus is based on standard Ethernet, the bus is directly accessible from Linux userspace using raw sockets (`SOCK_RAW`). Where possible we use a kernel with realtime patches for consistent timing of control loops running on the host. 

### Awesomed

Awesomed is a daemon process which provides a shared-memory interface to the bus. This allows for efficient node register read/write commands from control loop processes.

### Libabclient

Libabclient is a wrapper around awesomed for implementing control loop code. It provides a high-level bus interface to control nodes. Currently we have implemented libabclient-java, libabclient-c, and libabclient-python.


## Sources

Host-side awesomebus sources are available on [Bitbucket][bitbucket]

[bitbucket]: https://bitbucket.org/rng/awesomebus

</div>
