/* vim:set ts=2 sw=2 sts=2 et: */
#ifndef COMMON_H
#define COMMON_H

#include <stdlib.h>

#define LOGI(...) log_write(" INFO", __VA_ARGS__)
#define LOGW(...) log_write(" WARN", __VA_ARGS__)
#define LOGE(...) log_write("ERROR", __VA_ARGS__)

#define FATAL(...) { \
  printf("%s:%d: ",__FUNCTION__,__LINE__); \
  printf(__VA_ARGS__); \
  LOGE(__VA_ARGS__); \
  exit(1); \
}

void log_open(char *fn);
void log_close();
void log_write(char *i, char *fmt, ...);

#endif
