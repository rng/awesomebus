/* vim:set ts=2 sw=2 sts=2 et: */
#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include "common.h"
#include "core.h"

/**
 * Parse a sequence of ADPackets and generate the appropriate ABFrame sequence
 **/
void core_packets_to_frame(ADState *ad, ABFrame *frame, 
                           ADPacket *ipkts, int npkts)
{
  for (int i=0;i<npkts;i++) {
    ADPacket *c = &ipkts[i];
    // Process each packet, if it's an:
    // - AD_WRITE, do an AB_WRITE
    // - AD_READ, do an AB_READ
    // This is a bucket-per-command, which is inneficient
    switch (c->cmd) {
      case AD_WRITE:
        assert((c->data.w.reg >= 0) && (c->data.w.reg < 16));
        assert((c->data.w.reg >= 0) && (c->data.w.reg < 0x1000000));
        AB_ADD_BUCKET(frame, c->node, 
            AB_WRITE(c->data.w.reg, c->data.w.value));
        break;

      case AD_READ:
        assert((c->data.r.reg >= 0) && (c->data.r.reg < 16));
        AB_ADD_BUCKET(frame, c->node, 
            AB_READ(c->data.r.reg));
        break;

      default:
        FATAL("unhandled packet command: %d\n", c->cmd);
        break;
    }
  }
  // Add a 'DHCP' bucket at the end of every frame
  // frame_to_packets will check for this to detect new nodes.
  AB_ADD_BUCKET(frame, AB_BROADCAST_ADDR,
    AB_SETADDR(ad->free_node_address),
  );
} 


/**
 * Parse a sequence of ADFrame responses and generate the respective
 * ADPacket sequence
 **/
int core_frame_to_packets(ADState *ad, ABFrame *frame, 
                          uint32_t buckets[], ADPacket *pkts)
{
  int i = 0;
  int pkti = 0;
  uint32_t wordpos = 0;
  
  while (wordpos < frame->wordcount) {
    ABBucket *bkt = (ABBucket*)(((char*)frame) + buckets[i++]);
    // process this bucket
    if ((bkt->node == AB_BROADCAST_ADDR) && (bkt->length == 1)) {
      // if it's for node 0xFF, it is a 'DHCP' bucket, check if a new node
      // took the address
      if (bkt->instructions[0] == AB_NOP) {
        pkts[pkti].cmd = AD_NEWNODE;
        pkts[pkti].node = bkt->node;
        pkts[pkti].data.n.addr = ad->free_node_address;
        pkti++;
        ad->free_node_address++;
      }
    } else {
      // Otherwise it's a normal bucket, check for read results
      for (int j = 0; j < bkt->length; j++) {
        uint32_t inst = bkt->instructions[j];
        uint8_t op = inst & 0xF;
        if (op == AB_OP_READ) {
          pkts[pkti].cmd = AD_VALUE;
          pkts[pkti].node = bkt->node;
          pkts[pkti].data.v.reg = (inst>>4)&0xF;
          pkts[pkti].data.v.value = (inst>>8);
          pkti++;
        }
      }
    }
    wordpos += (1 + bkt->length);
  }
  // return number of packets
  return pkti;
}


/**
 * Process a sequence of ADPackets (ipkts), returning 
 * a sequence of ADPackets (opkts)
 **/
void core_process_packets(ADState *ad, ADPacket *ipkts, int npkts, 
                          ADPacket *opkts, int *nrpkts) 
{
  uint8_t pkt[1024];
  uint8_t rpkt[1024];
  uint32_t buckets[32];
  uint32_t len;
  int r;
  
  memset(pkt, 0, sizeof(pkt));
  memset(rpkt, 0, sizeof(rpkt));
  ABFrame *frame = (ABFrame*)pkt;
  ABFrame *rframe = (ABFrame*)rpkt;

  // create a ABFrame from a sequence of ADPackets
  ab_frame_create(frame);
  core_packets_to_frame(ad, frame, ipkts, npkts);
  ab_frame_finish(frame, &len, buckets);

  // process the frame through the awesomebus interface
  if ((r = ab_interface_process(&ad->abif, pkt, rpkt, len)) != 0) {
      FATAL("ab_interface_process failed: %d\n", r);
  }

  // decode the response ABFrame into a sequence of ADPackets
  ab_frame_decode(frame, rframe, buckets, 0);
  *nrpkts = core_frame_to_packets(ad, rframe, buckets, opkts);
}


/**
 * Initialise the bus, check for existing nodes with assigned addresses
 * and assign addresses tonew nodes
 **/
void core_start(ADState *ad) {
  uint8_t pkt[1024];
  uint8_t rpkt[1024];
  uint32_t buckets[32];
  uint32_t len;
  int done = false;
  int r;
  
  // check existing nodes
  while ((!done) && (ad->free_node_address < 0xFF)) {
    // clear frame
    memset(pkt, 0, sizeof(pkt));
    memset(rpkt, 0, sizeof(rpkt));
    ABFrame *frame = (ABFrame*)pkt;
    ABFrame *rframe = (ABFrame*)rpkt;
    // build frame with single ping
    ab_frame_create(frame);
    AB_ADD_BUCKET(frame, ad->free_node_address, AB_PING());
    ab_frame_finish(frame, &len, buckets);
    // process frame
    if ((r = ab_interface_process(&ad->abif, pkt, rpkt, len)) != 0) {
      FATAL("ab_interface_process failed: %d\n", r);
    }
    ab_frame_decode(frame, rframe, buckets, 0);
    // check results
    int wordpos = 0;
    int i = 0;
    while (wordpos < rframe->wordcount) {
      ABBucket *bkt = (ABBucket*)(((char*)rframe) + buckets[i++]);
      // process this bucket
      if ((bkt->node == ad->free_node_address) && (bkt->length == 1)) {
        if (bkt->instructions[0] == AB_NOP) {
          LOGI("Existing node found at %d\n", ad->free_node_address);
          ad->free_node_address++;
        } else {
          done = true;
        }
      }
      wordpos += (1 + bkt->length);
    }
  }

  // check new nodes
  for (int i=0;i<0xFF;i++) {
    int nrpkts;
    ADPacket opkts[32];
    core_process_packets(ad, NULL, 0, opkts, &nrpkts);
    if (nrpkts == 0)
      break;
    else
      LOGI("Assigned new node at %d\n", ad->free_node_address-1);
  }
  LOGI("Init complete %d nodes on bus\n", ad->free_node_address);
}
