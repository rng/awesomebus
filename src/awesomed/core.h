/* vim:set ts=2 sw=2 sts=2 et: */
#ifndef CORE_H
#define CORE_H

#include "awesomed.h"
#include <stdint.h>

#define AD_WRITE   (1)
#define AD_READ    (2)
#define AD_VALUE   (3)
#define AD_NEWNODE (4)

// Awesomed Packet structure - tagged union
typedef struct {
  uint32_t cmd;
  uint32_t node;
  union {
    // write
    struct { 
      uint32_t reg;
      uint32_t value;
    } w;
    // read
    struct {
      uint32_t reg;
    } r;
    // value
    struct {
      uint32_t reg;
      uint32_t value;
    } v;
    // new node
    struct {
      uint32_t addr;
    } n;
  } data;
} ADPacket;

void core_packets_to_frame(ADState *ad, ABFrame *frame, 
                           ADPacket *ipkts, int npkts);
int core_frame_to_packets(ADState *ad, ABFrame *frame, 
                          uint32_t buckets[], ADPacket *pkts);
void core_process_packets(ADState *ad, ADPacket *ipkts, int npkts, 
                          ADPacket *opkts, int *nrpkts);
void core_start(ADState *ad);

#endif
