/* vim:set ts=2 sw=2 sts=2 et: */
#ifndef AWESOMED_H
#define AWESOMED_H

#include "awesomebus.h"

typedef struct {
  char abdev[64];
  char shmdev[64];
  char shmif[8];

  ABInterface abif;

  int free_node_address;
} ADState;

#endif
