/* vim:set ts=2 sw=2 sts=2 et: */
#include "awesomebus.h"
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ipc_shm.h"
#include "common.h"
#include "core.h"


#define COMMANDS_READ  (0x1)
#define COMMANDS_WRITE (0x2)

int nextNodeNumber = 1;

int processDataFromClient(ABInterface *abif, char * buffer, int bufferLength, char * writeBuffer) {
  // create packet
  uint8_t pkt[1024];
  uint8_t rpkt[1024];
  uint32_t len;
  memset(pkt, 0, sizeof(pkt));
  memset(rpkt, 0, sizeof(rpkt));
  ABFrame *frame = (ABFrame*)pkt;
  ABFrame *rframe = (ABFrame*)rpkt;

  ab_frame_create(frame);

  int numBuckets = buffer[1];  
  int bufferCounter = 2;
  uint32_t bucketsProcessed = 0;

  uint32_t buckets[numBuckets];

  for (bucketsProcessed = 0; bucketsProcessed < numBuckets; bucketsProcessed++) {
    int numCommands = buffer[bufferCounter++];
    int node = buffer[bufferCounter++];
    int commandsProcessed = 0;
    for (commandsProcessed = 0; commandsProcessed < numCommands; commandsProcessed++) {
      char cmd = (buffer[bufferCounter]) & 0xF;
      char reg = (buffer[bufferCounter++] >> 4) & 0xf;
      if (cmd == COMMANDS_READ) {
        AB_ADD_BUCKET(frame, node, 
            AB_READ(reg)
            );
      } else if (cmd == COMMANDS_WRITE) {
        int value = buffer[bufferCounter++] << 16;
        value += buffer[bufferCounter++] << 8;
        value += buffer[bufferCounter++];
        AB_ADD_BUCKET(frame, node, 
            AB_WRITE(reg, value),
            );
      } else {
        FATAL("bad command %i\n", cmd);
      }
    }
  }

  AB_ADD_BUCKET(frame, 0xFF, 
    AB_SETADDR(nextNodeNumber), 
  );
  numBuckets++;

  ab_frame_finish(frame, &len, buckets);
  ab_interface_process(abif, pkt, rpkt, len);

  ab_frame_decode(frame, rframe, buckets, 0);

  // process returned packet
  uint32_t wordpos = 0;
  uint32_t i = 0; // run through buckets
  uint32_t indexIntoWriteBuffer = 1; // start writing at 1, first 1 byte(s) hold number of nodes
  while (wordpos < rframe->wordcount) {
    ABBucket *bkt = (ABBucket*)(((char*)rframe) + buckets[i++]);
    uint16_t node = bkt->node;
    uint16_t length = bkt->length;
    //printf(" Bucket for node 0x%04x, %d words:\n", node, length);

    for (int j = 0; j < (length); j++) {
      uint32_t inst = bkt->instructions[j];
      uint16_t op = inst & 0xF;
      //printf("  inst = %08x: op = 0x%x\n", inst, op);
      if ((op == AB_OP_NOP) && (bkt->node == 0xFF)) {
        printf("found new node %d\n", nextNodeNumber);
        nextNodeNumber++;
      } else if (op == AB_OP_READ) {
        // here write out a read register command to the file

        writeBuffer[indexIntoWriteBuffer++] = (node) & 0xFF;

        // we reorder so the command/reg byte is first to make this easier to parse
        writeBuffer[indexIntoWriteBuffer++] = (inst) & 0xFF;
        writeBuffer[indexIntoWriteBuffer++] = (inst >> 24) & 0xFF;
        writeBuffer[indexIntoWriteBuffer++] = (inst >> 16) & 0xFF;
        writeBuffer[indexIntoWriteBuffer++] = (inst >> 8) & 0xFF;
      }
    }
    wordpos += (1 + bkt->length);
  }
  //writeBuffer[0] = ((nextNodeNumber - 1) >> 8) & 0xF;
  writeBuffer[0] = (nextNodeNumber - 1) & 0xFF;

  return 0;
}

void main_shm(ADState *ad) {
  FILE* interfaceFile;

  int fileLength = 0;
  char * buffer;
  char * writeBuffer;
  int result;
  char * inputTriggerBuffer = (char*) malloc (sizeof(char));

  char triggerBuffer[] = {(char) 2};

  /* 
   * Set our umask to 0. The umask is the set of permission bits to disable in
   * new files. by setting it to 0, new files are created 0666 (world rw).
   * This works for now, but we should probably set up a better system with 
   * groups
   */
  umask(0);

  interfaceFile = fopen(ad->shmdev, "w+b");  
  if (!interfaceFile) {
    FATAL("Couldn't create SHM file\n");
  }

  ABInterface abif;
  if (ab_interface_open(&abif, ad->abdev) != AB_ERR_OK) {
    FATAL("Couldn't open awesomebus interface\n");
  }

  int allocated = 0;
  buffer = NULL;
  writeBuffer = NULL;
  while(1) {    
    do {
      rewind(interfaceFile);
      fflush(interfaceFile);
      result = fread(inputTriggerBuffer, 1, 1, interfaceFile);
    } while(inputTriggerBuffer[0] != 1);


    fflush(interfaceFile);
    fseek(interfaceFile, 0, SEEK_END);

    fileLength = ftell(interfaceFile);
    rewind(interfaceFile);    

    // allocate memory to contain the whole file:
    if (allocated == 0) {
      buffer = (char*) malloc (sizeof(char)*fileLength);
      writeBuffer = (char*) malloc (sizeof(char)*fileLength);
      allocated = 1;
      if (buffer == NULL) {
        FATAL("Memory error");
      }
    }

    // copy the file into a buffer:
    result = fread (buffer, 1, fileLength, interfaceFile);
    if (result != fileLength) {
      FATAL("Reading error");
    }

    processDataFromClient(&abif, buffer, fileLength, writeBuffer);

    // first seek to 1th byte (not 0th) so we write the data after the trigger byte
    fseek(interfaceFile, 1, SEEK_SET);

    fwrite(writeBuffer, 1, fileLength-1, interfaceFile);
    fflush(interfaceFile);

    // after writing data, go back to 0th byte and set it so the client knows the data is ready
    fseek(interfaceFile, 0, SEEK_SET);
    fwrite(triggerBuffer, 1, 1, interfaceFile);

    fflush(interfaceFile);

  }

  fclose(interfaceFile);
  ab_interface_close(&abif);

}
