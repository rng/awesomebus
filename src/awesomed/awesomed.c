/* vim:set ts=2 sw=2 sts=2 et: */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <getopt.h>
#include <sched.h>
#include "ipc_shm.h"
#include "ipc_mq.h"
#include "common.h"
#include "errno.h"
#include "awesomed.h"


void usage() {
  printf("usage: awesomed <options>\n");
  printf("options:\n");
  printf(" -i <interface> : hardware interface name (default awesome0)\n");
  printf(" -f <ipc file>  : IPC interface filename (default /awesomed)\n");
  printf(" -s [shm|mq]    : IPC type (default mq)\n");
  printf(" -h             : this text\n");
  exit(1);
}


int main(int argc, char *argv[]) {
  ADState ad;

  strcpy(ad.abdev, "awesome0");
  strcpy(ad.shmdev, "/awesomed");
  strcpy(ad.shmif, "mq");
  ad.free_node_address = 0;

  int c;

  while ((c = getopt (argc, argv, "i:f:s:h")) != -1) {
    switch (c) {
      case 'i':
        strncpy(ad.abdev, optarg, 63);
        break;
      case 'f':
        strncpy(ad.shmdev, optarg, 63);
        break;
      case 's':
        strncpy(ad.shmif, optarg, 7);
        break;
      default:
        usage();
    }
  }

  log_open("awesomed.log");
  LOGI("Awesomed starting on interface %s, IPC is %s:%s\n", 
       ad.abdev, ad.shmif, ad.shmdev);
     
  // use FIFO scheduler with high priority
  struct sched_param sp;
  if (sched_getscheduler(0) == -1) {
    FATAL("couldn't get scheduler: %s\n", strerror(errno));
  }
  sp.sched_priority = sched_get_priority_max(SCHED_FIFO);
  sched_setscheduler(0, SCHED_FIFO, &sp);

  if (strcmp(ad.shmif, "mq")==0) {
    main_mq(&ad);
  } else if (strcmp(ad.shmif, "shm")==0) {
    main_shm(&ad);
  } else {
    usage();
  }

  log_close();

  return 0;
}
