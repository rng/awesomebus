/* vim:set ts=2 sw=2 sts=2 et: */
#include <arpa/inet.h>
#include "awesomebus.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mqueue.h>
#include <assert.h>
#include "common.h"
#include "ipc_mq.h"
#include "errno.h"
#include "core.h"


#define MQ_MODE S_IRUSR|S_IWUSR 
#define MQMSG_SIZE (256)


/**
 * Read a sequence of ADPackets from the queue, process them through the
 * Awesomebus interface, then write out a bunch of response ADPackets.
 **/
void process_messages(ADState *ad, mqd_t mqc, mqd_t mqr) {
  uint32_t priority;
  ADPacket ipkts[32];
  ADPacket opkts[32];
  
  // read command packets from queue
  int recv = mq_receive(mqc, (char*)ipkts, sizeof(ipkts), &priority);
  if (recv == -1) {
    FATAL("mq_receive failed: %s\n", strerror(errno));
  }

  // process packets through awesomebus
  int npkts = recv / sizeof(ADPacket);
  int nrpkts = 0;
  core_process_packets(ad, ipkts, npkts, opkts, &nrpkts);

  // write response packets to queue
  if (mq_send(mqr, (char*)opkts, nrpkts*sizeof(ADPacket), 1) == -1) {
    FATAL("mq_send failed: %s\n", strerror(errno));
  }
}


void main_mq(ADState *ad) {
  mqd_t mqc, mqr;
  struct mq_attr attr;
  char cmdq_name[64];
  char rspq_name[64];

  // set mq attributes
  attr.mq_maxmsg = 8;
  attr.mq_msgsize = MQMSG_SIZE;
  attr.mq_flags = 0;
  attr.mq_curmsgs = 0;
  
  // build queue names
  snprintf(cmdq_name, 64, "%s_cmd.1", ad->shmdev);
  snprintf(rspq_name, 64, "%s_rsp.1", ad->shmdev);
  
  // unlink any exisiting mq
  mq_unlink(cmdq_name);
  mq_unlink(rspq_name);

  // open mq
  if ((mqc = mq_open(cmdq_name, O_RDWR|O_CREAT, MQ_MODE, &attr)) < 0) {
    FATAL("Couldn't open command message queue: %s\n", strerror(errno));
  }
  if ((mqr = mq_open(rspq_name, O_RDWR|O_CREAT, MQ_MODE, &attr)) < 0) {
    FATAL("Couldn't open response message queue: %s\n", strerror(errno));
  }

  // open awesomebus
  if (ab_interface_open(&ad->abif, ad->abdev) != AB_ERR_OK) {
    FATAL("Couldn't open awesomebus interface\n");
  }

  // initialise our state and the bus
  core_start(ad);

  // mainloop
  while(1) {
    process_messages(ad, mqc, mqr);
  }

  // done
  ab_interface_close(&ad->abif);
  mq_close(mqc);
  mq_close(mqr);
}
