/* vim:set ts=2 sw=2 sts=2 et: */
#include <string.h>
#include <stdarg.h>
#include <stdio.h>
#include <errno.h>
#include "common.h"

static FILE *logfile = NULL;

void log_open(char *fn) {
  logfile = fopen(fn, "a+");
  if (!logfile)
    FATAL("Couldn't open logfile '%s': %s\n", fn, strerror(errno));
}

void log_close() {
  fclose(logfile);
}

void log_write(char *i, char *fmt, ...) {
  if (logfile) {
    va_list args;
    va_start(args, fmt);
    fprintf(logfile, "%s: ", i);
    vfprintf(logfile, fmt, args);
    fflush(logfile);
    va_end(args);
  }
}

