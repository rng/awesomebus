package abClient.util;

import java.io.*;
import java.util.List;

public class SystemCommandExecutor {
	private List<String> commandInformation;
	private String adminPassword;
	private ThreadedStreamHandler inputStreamHandler;
	private ThreadedStreamHandler errorStreamHandler;
	private boolean echoStreams;
	private boolean shouldGobbleStreams = true;

	public boolean isShouldGobbleStreams() {
		return shouldGobbleStreams;
	}

	public String getFirstLineOutputted() {
		return inputStreamHandler.getFirstLineFound();
	}

	public void setShouldGobbleStreams(boolean shouldGobbleStreams) {
		this.shouldGobbleStreams = shouldGobbleStreams;
		if (inputStreamHandler != null) {
			inputStreamHandler.setShouldGobbleStreams(shouldGobbleStreams);
		}
		if (errorStreamHandler != null) {
			errorStreamHandler.setShouldGobbleStreams(shouldGobbleStreams);
		}
	}

	/**
	 * Pass in the system command you want to run as a List of Strings, as shown
	 * here:
	 * 
	 * List<String> commands = new ArrayList<String>();
	 * commands.add("/sbin/ping"); commands.add("-c"); commands.add("5");
	 * commands.add("www.google.com"); SystemCommandExecutor commandExecutor =
	 * new SystemCommandExecutor(commands); commandExecutor.executeCommand();
	 * 
	 * Note: I've removed the other constructor that was here to support
	 * executing the sudo command. I'll add that back in when I get the sudo
	 * command working to the point where it won't hang when the given password
	 * is wrong.
	 * 
	 * @param commandInformation
	 *            The command you want to run.
	 */
	public SystemCommandExecutor(final List<String> commandInformation,
			boolean echoStreams) {
		if (commandInformation == null)
			throw new NullPointerException(
					"The commandInformation is required.");
		this.commandInformation = commandInformation;
		this.adminPassword = null;
		this.echoStreams = echoStreams;
	}

	public SystemCommandExecutor(final List<String> commandInformation,
			String adminPassword, boolean echoStreams) {
		if (commandInformation == null)
			throw new NullPointerException(
					"The commandInformation is required.");
		this.commandInformation = commandInformation;
		this.adminPassword = adminPassword;
		this.echoStreams = echoStreams;
	}

	private Process process;

	public int startExecutingCommand() throws IOException, InterruptedException {
		int exitValue = -99;

		try {
			ProcessBuilder pb = new ProcessBuilder(commandInformation);
			process = pb.start();

			// you need this if you're going to write something to the command's
			// input stream
			// (such as when invoking the 'sudo' command, and it prompts you for
			// a password).
			// OutputStream stdOutput = process.getOutputStream();
			OutputStream stdOutput = null;

			// i'm currently doing these on a separate line here in case i need
			// to set them to null
			// to get the threads to stop.
			// see
			// http://java.sun.com/j2se/1.5.0/docs/guide/misc/threadPrimitiveDeprecation.html
			InputStream inputStream = process.getInputStream();
			InputStream errorStream = process.getErrorStream();

			if (inputStream == null) {
				System.err
						.println("SystemCommandExecutor.executeCommand() doesn't have input stream");
				throw new NullPointerException(
						"the process appears to not have created an input stream!");
			}
			if (errorStream == null) {
				System.err
						.println("SystemCommandExecutor.executeCommand() doesn't have error stream");
				throw new NullPointerException(
						"the process appears to not have created an error stream!");
			}

			// these need to run as java threads to get the standard output and
			// error from the command.
			// the inputstream handler gets a reference to our stdOutput in case
			// we need to write
			// something to it, such as with the sudo command
			if (echoStreams) {
				// System.err
				// .println("SystemCommandExecutor.executeCommand() creating stream handelers with echoing");
				inputStreamHandler = new ThreadedStreamHandler(inputStream,
						stdOutput, adminPassword, System.out);
				errorStreamHandler = new ThreadedStreamHandler(errorStream,
						System.err);
			} else {
				// System.err
				// .println("SystemCommandExecutor.executeCommand() creating stream handelers without echoing");
				inputStreamHandler = new ThreadedStreamHandler(inputStream,
						stdOutput, adminPassword);
				errorStreamHandler = new ThreadedStreamHandler(errorStream);
			}

			inputStreamHandler.setShouldGobbleStreams(shouldGobbleStreams);
			errorStreamHandler.setShouldGobbleStreams(shouldGobbleStreams);

			 inputStreamHandler.setDaemon(true);
			 errorStreamHandler.setDaemon(true);

			inputStreamHandler.start();
			errorStreamHandler.start();

		} catch (IOException e) {
			e.printStackTrace();
			throw e;
		}
		return exitValue;
	}

	public void killProcess() {
		// try {
		 inputStreamHandler.interrupt();
		 errorStreamHandler.interrupt();
		// inputStreamHandler.join();
		// errorStreamHandler.join();
		process.destroy();

		// try {
		// process.waitFor();
		// } catch (InterruptedException e) {
		// e.printStackTrace();
		// }

		// } catch (InterruptedException e1) {
		// e1.printStackTrace();
		// throw new RuntimeException(e1);
		// }

	}

	public int waitForProcessToEnd() {
		int exitValue = -99;
		try {
			exitValue = process.waitFor();

			inputStreamHandler.interrupt();
			errorStreamHandler.interrupt();
			inputStreamHandler.join();
			errorStreamHandler.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		return exitValue;
	}

	private int executeCommand() throws IOException, InterruptedException {
		int exitValue = -99;

		try {
			ProcessBuilder pb = new ProcessBuilder(commandInformation);
			Process process = pb.start();

			// you need this if you're going to write something to the command's
			// input stream
			// (such as when invoking the 'sudo' command, and it prompts you for
			// a password).
			OutputStream stdOutput = process.getOutputStream();

			// i'm currently doing these on a separate line here in case i need
			// to set them to null
			// to get the threads to stop.
			// see
			// http://java.sun.com/j2se/1.5.0/docs/guide/misc/threadPrimitiveDeprecation.html
			InputStream inputStream = process.getInputStream();
			InputStream errorStream = process.getErrorStream();

			if (inputStream == null) {
				System.err
						.println("SystemCommandExecutor.executeCommand() doesn't have input stream");
				throw new NullPointerException(
						"the process appears to not have created an input stream!");
			}
			if (errorStream == null) {
				System.err
						.println("SystemCommandExecutor.executeCommand() doesn't have error stream");
				throw new NullPointerException(
						"the process appears to not have created an error stream!");
			}

			// these need to run as java threads to get the standard output and
			// error from the command.
			// the inputstream handler gets a reference to our stdOutput in case
			// we need to write
			// something to it, such as with the sudo command
			if (echoStreams) {
				// System.err
				// .println("SystemCommandExecutor.executeCommand() creating stream handelers with echoing");
				inputStreamHandler = new ThreadedStreamHandler(inputStream,
						stdOutput, adminPassword, System.out);
				errorStreamHandler = new ThreadedStreamHandler(errorStream,
						System.err);
			} else {
				// System.err
				// .println("SystemCommandExecutor.executeCommand() creating stream handelers without echoing");
				inputStreamHandler = new ThreadedStreamHandler(inputStream,
						stdOutput, adminPassword);
				errorStreamHandler = new ThreadedStreamHandler(errorStream);
			}

			inputStreamHandler.start();
			errorStreamHandler.start();

			exitValue = process.waitFor();

			inputStreamHandler.interrupt();
			errorStreamHandler.interrupt();
			inputStreamHandler.join();
			errorStreamHandler.join();

		} catch (IOException e) {
			e.printStackTrace();
			throw e;
		} catch (InterruptedException e) {
			// generated by process.waitFor() call
			e.printStackTrace();
			throw e;
		} finally {
			// if (errorStreamHandler == null) {
			// throw new NullPointerException(
			// "failed to create the error stream handler");
			// }
			return exitValue;
		}
	}

	/**
	 * Get the standard output (stdout) from the command you just exec'd.
	 */
	public StringBuilder getStandardOutputFromCommand() {
		return inputStreamHandler.getOutputBuffer();
	}

	/**
	 * Get the standard error (stderr) from the command you just exec'd.
	 */
	public StringBuilder getStandardErrorFromCommand() {
		return errorStreamHandler.getOutputBuffer();
	}

}
