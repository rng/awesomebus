package abClient.util;

import java.util.ArrayList;

public class ByteUtils {

	public static final byte[] intToByteArray(int value) {
		return new byte[] { (byte) (value >>> 24), (byte) (value >>> 16),
				(byte) (value >>> 8), (byte) value };
	}

	public static final short[] combineShortArrays(short[] a, short[] b) {
		short[] ret = new short[a.length + b.length];
		System.arraycopy(a, 0, ret, 0, a.length);
		System.arraycopy(b, 0, ret, a.length, b.length);
		return ret;
	}

	public static final short[] combineShortArrays(ArrayList<short[]> as) {
		int length = 0;
		for (short[] a : as) {
			length += a.length;
		}
		short[] ret = new short[length];
		int i = 0;
		for (short[] a : as) {
			System.arraycopy(a, 0, ret, i, a.length);
			i += a.length;
		}
		return ret;
	}

}
