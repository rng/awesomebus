package abClient.util.test;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import abClient.util.ByteUtils;


public class TestByteUtils {

	@Before
	public void setUp() throws Exception {
		
	}

	@Test
	public void testCombineShortArraysShortArrayShortArray() {

		short[] a = new short[] { 1, 2, 3, 4, 5 };
		short[] b = new short[] { 6, 7, 8 };

		short[] c = new short[] { 1, 2, 3, 4, 5, 6, 7, 8 };

		short[] result = ByteUtils.combineShortArrays(a, b);
		assertArrayEquals(c, result);
	}

	@Test
	public void testCombineShortArraysArrayListOfshort() {
		short[] a = new short[] { 1, 2 };
		short[] b = new short[] { 3, 4 };
		short[] c = new short[] { 5 };
		short[] d = new short[] { 6, 7 };
		short[] e = new short[] { 8 };

		short[] expected = new short[] { 1, 2, 3, 4, 5, 6, 7, 8 };

		ArrayList<short[]> allArrays = new ArrayList<short[]>();

		allArrays.add(a);
		allArrays.add(b);
		allArrays.add(c);
		allArrays.add(d);
		allArrays.add(e);

		short[] result = ByteUtils.combineShortArrays(allArrays);
		assertArrayEquals(expected, result);
	}

}
