package abClient.util;

import java.io.*;

public class ThreadedStreamHandler extends Thread {
	private InputStream inputStream;
	private String adminPassword;
	// private OutputStream outputStream;
	// private PrintWriter printWriter;
	private StringBuilder outputBuffer = new StringBuilder();
	private boolean sudoIsRequested = false;
	private PrintStream echoStream;
	private boolean shouldGobbleStreams = true;
	private String firstLineFound;

	public String getFirstLineFound() {
		return firstLineFound;
	}

	public boolean isShouldGobbleStreams() {
		return shouldGobbleStreams;
	}

	public void setShouldGobbleStreams(boolean shouldGobbleStreams) {
		this.shouldGobbleStreams = shouldGobbleStreams;
	}

	/**
	 * A simple constructor for when the sudo command is not necessary. This
	 * constructor will just run the command you provide, without running sudo
	 * before the command, and without expecting a password.
	 * 
	 * @param inputStream
	 * @param streamType
	 */
	public ThreadedStreamHandler(InputStream inputStream, PrintStream echoStream) {
		if (inputStream == null) {
			System.err
					.println("ThreadedStreamHandler.ThreadedStreamHandler(), null pointer, given input stream!");
			throw new NullPointerException("given input stream cannot be null!");
		}
		this.inputStream = inputStream;
		this.echoStream = echoStream;
	}

	public ThreadedStreamHandler(InputStream inputStream) {
		this(inputStream, null);
	}

	/**
	 * Use this constructor when you want to invoke the 'sudo' command. The
	 * outputStream must not be null. If it is, you'll regret it. :)
	 * 
	 * TODO this currently hangs if the admin password given for the sudo
	 * command is wrong.
	 * 
	 * @param inputStream
	 * @param streamType
	 * @param outputStream
	 * @param adminPassword
	 */
	public ThreadedStreamHandler(InputStream inputStream,
			OutputStream outputStream, String adminPassword,
			PrintStream echoStream) {
		if (inputStream == null) {
			throw new NullPointerException("given input stream cannot be null!");
		}
		this.inputStream = inputStream;
		// this.outputStream = outputStream;
		// this.printWriter = new PrintWriter(outputStream);
		this.adminPassword = adminPassword;
		this.sudoIsRequested = true;
	}

	public ThreadedStreamHandler(InputStream inputStream,
			OutputStream outputStream, String adminPassword) {
		this(inputStream, outputStream, adminPassword, null);
		if (this.inputStream == null) {
			throw new NullPointerException("given input stream cannot be null!");
		}
	}

	public void run() {

		if (inputStream == null) {
			throw new NullPointerException(
					"input stream can't be null in a running threaded stream handler!");
		}

		// on mac os x 10.5.x, when i run a 'sudo' command, i need to write
		// the admin password out immediately; that's why this code is
		// here.
		// if (sudoIsRequested) {
		// // while (true) {
		// // System.out
		// // .println("ThreadedStreamHandler.run() sending out sudo password");
		// // doSleep(500);
		// printWriter.println(adminPassword);
		// printWriter.flush();
		// doSleep(5);
		// // }
		// }

		BufferedReader bufferedReader = null;
		if (shouldGobbleStreams) {
			try {
				bufferedReader = new BufferedReader(new InputStreamReader(
						inputStream));
				String line = null;
				// System.out
				// .println("ThreadedStreamHandler.run() waiting for input");
				while ((line = bufferedReader.readLine()) != null) {
					outputBuffer.append(line + "\n");
					if (firstLineFound == null) {
						firstLineFound = line;
					}
					// System.err
					// .println("ThreadedStreamHandler.run() appending to reader: "
					// + line);
					if (echoStream != null) {
						echoStream.println(line);
					}
				}
			} catch (IOException ioe) {
				// ioe.printStackTrace();
			} catch (Throwable t) {
				// t.printStackTrace();
			} finally {
				try {
					bufferedReader.close();
				} catch (IOException e) {
				}
			}
		}
	}

	private void doSleep(long millis) {
		try {
			Thread.sleep(millis);
		} catch (InterruptedException e) {
		}
	}

	public StringBuilder getOutputBuffer() {
		return outputBuffer;
	}

}
