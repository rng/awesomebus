package abClient;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel.MapMode;
import java.util.ArrayList;
import java.util.concurrent.TimeoutException;

import abClient.commands.AwesomebusBucket;
import abClient.commands.AwesomebusCommand;
import abClient.commands.AwesomebusFrame;
import abClient.commands.responses.AwesomebusCommandResponse;
import abClient.commands.responses.AwesomebusCommandResponseParser;
import abClient.util.ByteUtils;


public class AwesomebusJavaInterface {

	public static final String DEFAULT_SHARED_MEMORY_FILENAME = "/dev/shm/awesomebus-shm";

	public static final int DEFAULT_MAX_BUFFER_SIZE = 1024;

	private AwesomebusFrame awesomebusFrame;
	private final String sharedMemoryFilename;
	private final int maxBufferSize;
	private byte[] signedBytesToRead;
	private short[] bytesToRead;
	private RandomAccessFile interfaceFile;
	private MappedByteBuffer mappedByteBuffer;
	private byte[] triggerBytes;

	private boolean isWaitingForReply;
	private int numberOfNodesFound = 0;
	private ArrayList<AwesomebusCommandResponse> commandResults = new ArrayList<AwesomebusCommandResponse>();

	public AwesomebusJavaInterface() {
		this(DEFAULT_SHARED_MEMORY_FILENAME, DEFAULT_MAX_BUFFER_SIZE);
	}

	public AwesomebusJavaInterface(String sharedMemoryFilename,
			int maxBufferSize) {
		this.sharedMemoryFilename = sharedMemoryFilename;
		this.maxBufferSize = maxBufferSize;
		isWaitingForReply = false;
	}

	public void initialize() throws IOException {

		File f = new File(sharedMemoryFilename);
		(new File(f.getParent())).mkdirs();

		interfaceFile = new RandomAccessFile(sharedMemoryFilename, "rw");
		interfaceFile.seek(0);
		interfaceFile.write(0);
		mappedByteBuffer = interfaceFile.getChannel().map(MapMode.READ_WRITE,
				0, maxBufferSize + 2);

		signedBytesToRead = new byte[maxBufferSize];
		bytesToRead = new short[maxBufferSize];
		triggerBytes = new byte[1];

		isWaitingForReply = false;
		commandResults = new ArrayList<AwesomebusCommandResponse>();
		numberOfNodesFound = 0;
	}

	public static boolean checkBytesAreInRange(short[] bytes) {
		for (short s : bytes) {
			if ((s < 0) || (s > 255)) {
				return false;
			}
		}
		return true;
	}

	public void sendCommandsToBus() throws IOException {
		isWaitingForReply = false;
		if (triggerBytes == null) {
			throw new RuntimeException(
					"Must initialize the AwesomebusJavaInterface before sending commands to bus");
		}

		if (getAwesomebusFrame() == null) {
			throw new RuntimeException(
					"Must set the frame of an AwesomebusJavaInterface before sending commands to bus");
		}

		boolean useMappedByteBufferForWriting = false; // true; //

		short[] shortsToWrite = getAwesomebusFrame().getBytes();

		byte[] bytesToWrite = new byte[shortsToWrite.length];
		for (int i = 0; i < bytesToWrite.length; i++) {
			bytesToWrite[i] = (byte) shortsToWrite[i];
		}

		triggerBytes[0] = 0;
		if (useMappedByteBufferForWriting) {
			mappedByteBuffer.rewind();
			mappedByteBuffer.put(0, triggerBytes[0]);
			mappedByteBuffer.put(bytesToWrite, 1, bytesToWrite.length - 1);
			triggerBytes[0] = 1;
			mappedByteBuffer.put(0, triggerBytes[0]);
			mappedByteBuffer.force();
		} else {
			interfaceFile.seek(0);
			interfaceFile.write(triggerBytes);
			interfaceFile.write(bytesToWrite);
			interfaceFile.seek(0);
			triggerBytes[0] = 1;
			interfaceFile.write(triggerBytes);
			interfaceFile.getChannel().force(true);

			// System.err.print("bytes to write: ");
			// for (byte thisByte : bytesToWrite) {
			// System.err.print(thisByte + ", ");
			// }
			// System.err.println();
		}
		isWaitingForReply = true;
	}

	public AwesomebusFrame getAwesomebusFrame() {
		return awesomebusFrame;
	}

	public void setAwesomebusFrame(AwesomebusFrame awesomebusFrame) {
		this.awesomebusFrame = awesomebusFrame;
	}

	public void waitForReplyFromBus(long timeoutMS) throws TimeoutException,
			IOException {

		boolean useMappedByteBufferForCheckingTrigger = true;
		boolean useMappedByteBufferForReading = true;

		// now we play the waiting game. The master code is sending the
		// packet over the wire, when it is done, it will create a
		// packetRecievedFile signalling that we can load in the
		// returned packet data.
		// startTimeWaiting = System.nanoTime();

		long startTime = System.currentTimeMillis();
		// System.out
		// .println("TestSimpleSharedMemory.main() waiting for response");
		while (triggerBytes[0] == 1) {

			if (System.currentTimeMillis() > (startTime + timeoutMS)) {
				throw new TimeoutException(
						"timed out after "
								+ timeoutMS
								+ "ms while waiting for awesomed interface file to get populated");
			}

			if (useMappedByteBufferForCheckingTrigger) {
				mappedByteBuffer.load();
				triggerBytes[0] = mappedByteBuffer.get(0);
			} else {
				// interfaceFile.close();
				// interfaceFile = new RandomAccessFile(
				// sharedMemoryDirectoryName + "awesomebus-shm",
				// "rw");

				interfaceFile.seek(0);
				interfaceFile.getChannel().force(true);
				interfaceFile.read(triggerBytes);
			}

			// Thread.yield();
			// System.out
			// .println("TestSimpleSharedMemory.main() checking trigger "
			// + triggerBytes[0]);
		}

		if (useMappedByteBufferForReading) {
			mappedByteBuffer.rewind();
			mappedByteBuffer.get();
			mappedByteBuffer.get(signedBytesToRead, 0,
					signedBytesToRead.length - 1);
		} else {
			interfaceFile.seek(1L);
			interfaceFile.read(signedBytesToRead);
		}

		// System.out.print("response packet: ");
		// for (byte b : signedBytesToRead) {
		// System.out.print(b + ", ");
		// }
		// System.out.println();

		for (int i = 0; i < signedBytesToRead.length; i++) {
			bytesToRead[i] = byteToUnsignedByte(signedBytesToRead[i]);
		}

		numberOfNodesFound = bytesToRead[0];

		int offset = 1;

		// AwesomebusCommandResponse response = AwesomebusCommandResponse
		// .parseFromBytes(bytesToRead, offset);
		AwesomebusCommandResponseParser parser = new AwesomebusCommandResponseParser();
		AwesomebusCommandResponse response = parser
				.parseCommand(bytesToRead, 1);

		commandResults.clear();
		while (response != null) {
			commandResults.add(response);
			// response = AwesomebusCommandResponse.parseFromBytes(bytesToRead);
			response = parser.parseCommand(bytesToRead);
		}
		isWaitingForReply = false;
	}

	public static short byteToUnsignedByte(byte b) {
		return (short) (((short) b) & 0XFF);
	}

	public boolean isWaitingForReply() {
		return isWaitingForReply;
	}

	public ArrayList<AwesomebusCommandResponse> getCommandResults() {
		return commandResults;
	}

	public int getNumberOfNodesFound() {
		return numberOfNodesFound;
	}
}
