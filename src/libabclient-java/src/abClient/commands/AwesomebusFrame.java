package abClient.commands;

import java.util.ArrayList;

import abClient.util.ByteUtils;


public class AwesomebusFrame extends ArrayList<AwesomebusBucket> {

	public AwesomebusFrame() {
		super();
	}

	public void addCommand(AwesomebusCommand awesomebusCommand) {
		// This just creates a bucket to wrap around the command
		AwesomebusBucket awesomebusBucket = new AwesomebusBucket();
		awesomebusBucket.addCommand(awesomebusCommand);
		add(awesomebusBucket);
	}

	/**
	 * this takes all of the buckets in this frame that are addressed to node n,
	 * and creates a single bucket addressed to node n with all of the commands
	 * in the existing buckets addressed to n. It does this for all targeted
	 * nodes in the frame.
	 */
	public void condenseCommandsIntoUniqueBuckets() {
		throw new RuntimeException(
				"this isn't hard, but it won't do anything until awesomed is slightly less retarded");
	}

	public short[] getBytes() {
		ArrayList<short[]> allByteArrays = new ArrayList<short[]>();

		short numBuckets = (short) size();
		allByteArrays.add(new short[] { numBuckets });

		for (AwesomebusBucket bucket : this) {
			allByteArrays.add(bucket.getBytes());
		}

		short[] combinedBytes = ByteUtils.combineShortArrays(allByteArrays);
		return combinedBytes;
	}

	public String toString() {
		String ret = "AwesomebusFrame ";
		short[] bytes = getBytes();

		ret += bytes.length + " bytes: {";

		for (int i = 0; i < bytes.length; i++) {
			ret += bytes[i] + ", ";
		}
		ret += "}\n";
		return ret;
	}

}
