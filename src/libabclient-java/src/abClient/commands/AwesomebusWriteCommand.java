package abClient.commands;

import abClient.AwesomebusJavaInterface;
import abClient.util.ByteUtils;

public class AwesomebusWriteCommand extends AwesomebusCommand {

	private static final int MAX_CONTROL_VALUE = (int) ((Math.pow(2, 24)) - 1);
	private static final int MIN_CONTROL_VALUE = 0;
	private RegisterID registerID;
	private int valueToWrite;

	public AwesomebusWriteCommand(int nodeNumber, int registerNumber,
			int valueToWrite) {
		this(NodeID.getNodeID(nodeNumber), RegisterID
				.getRegisterID(registerNumber), valueToWrite);
	}

	public AwesomebusWriteCommand(NodeID nodeID, RegisterID registerID,
			int valueToWrite) {
		super(AwesomebusCommandType.WRITE, nodeID);
		this.registerID = registerID;
		this.valueToWrite = valueToWrite;
	}

	public static short[] getBytesFromValue(int value) {
		if (value > MAX_CONTROL_VALUE) {
			throw new IllegalArgumentException(
					"cannot get bytes from the value " + value
							+ ", since it is larger than the max allowable, "
							+ MAX_CONTROL_VALUE);
		}
		if (value < MIN_CONTROL_VALUE) {
			throw new IllegalArgumentException(
					"cannot get bytes from the value " + value
							+ ", since it is smaller than the min allowable, "
							+ MIN_CONTROL_VALUE);
		}

		return new short[] { (short) ((value >> 16) & 0xFF),
				(short) ((value >> 8) & 0xFF), (short) ((value) & 0xFF), };
	}

	public static int getValueFromBytes(short[] shorts) {
		if (!AwesomebusJavaInterface.checkBytesAreInRange(shorts)) {
			throw new IllegalArgumentException(
					"can't get value from bytes, at least one byte is out of range");
		}

		int ret = shorts[0] << 16;
		ret += shorts[1] << 8;
		ret += shorts[2];
		return ret;
	}

	@Override
	public short[] getCommandArgumentBytes() {
		short[] ret = ByteUtils.combineShortArrays(registerID.getBytes(),
				getBytesFromValue(getValueToWrite()));
		return ret;
	}

	public int getValueToWrite() {
		return valueToWrite;
	}

	public void setValueToWrite(int valueToWrite) {
		this.valueToWrite = valueToWrite;
	}
}
