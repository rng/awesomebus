package abClient.commands;

import abClient.util.ByteUtils;

public abstract class AwesomebusCommand {

	protected final NodeID nodeID;

	private final AwesomebusCommandType awesomebusCommandType;

	private static final int MAX_COMMAND_VALUE = 15;

	protected AwesomebusCommand(AwesomebusCommandType awesomebusCommandType,
			NodeID nodeID) {
		this.awesomebusCommandType = awesomebusCommandType;
		this.nodeID = nodeID;
	}

	public short[] getCommandBytes() {
		short[] commandBytes = awesomebusCommandType
				.getByteIdentifierOfCommand();

		if (commandBytes.length != 1) {
			throw new RuntimeException(
					"currently we expect 1 byte defining the command");
		}

		if (commandBytes[0] > MAX_COMMAND_VALUE) {
			throw new RuntimeException(
					"currently the command byte must not exceed"
							+ MAX_COMMAND_VALUE);
		}

		if (nodeID.getBytes().length != 1) {
			throw new RuntimeException(
					"currently we expect 1 byte defining the node ID");
		}

		// commandBytes[0] = (short) (commandBytes[0] + (nodeID.getBytes()[0] <<
		// 4));

		short[] argBytes = getCommandArgumentBytes();
		checkBytes(argBytes);

		commandBytes[0] = (short) (commandBytes[0] + (argBytes[0] << 4));

		short[] ret = commandBytes;
		if (argBytes.length > 1) {
			short[] argBytesMinusFirst = new short[argBytes.length - 1];
			System.arraycopy(argBytes, 1, argBytesMinusFirst, 0,
					argBytesMinusFirst.length);
			ret = ByteUtils
					.combineShortArrays(commandBytes, argBytesMinusFirst);
		}

		return ret;

	}

	protected abstract short[] getCommandArgumentBytes();

	private boolean checkBytes(short[] bytes) {
		for (short s : bytes) {
			if ((s < 0) || (s > 255)) {
				throw new RuntimeException(
						"tried to specify a byte that is outta range!");
			}
		}
		return true;
	}

	public NodeID getNodeID() {
		return nodeID;
	}

}
