package abClient.commands;

public class RegisterID {

	private int registerNumber;
	private short[] registerBytes;

	private static final int MAX_REGISTER_NUMBER = 23;
	private static RegisterID[] registerIds = new RegisterID[MAX_REGISTER_NUMBER + 1];

	private RegisterID(int registerNumber) {
		this.registerNumber = registerNumber;
		registerBytes = new short[1];
		registerBytes[0] = (short) registerNumber;
	}

	/**
	 * registerNumbers are 0 based.
	 */
	public static RegisterID getRegisterID(int registerNumber) {
		if (registerNumber < 0) {
			throw new IllegalArgumentException(
					"registerNumber must be >= 0, was " + registerNumber);
		}
		if (registerNumber > MAX_REGISTER_NUMBER) {
			throw new IllegalArgumentException(
					"registerNumber must not be larger than max register number ("
							+ MAX_REGISTER_NUMBER + "), was " + registerNumber);
		}
		if (registerIds[registerNumber] == null) {
			registerIds[registerNumber] = new RegisterID(registerNumber);
		}
		return registerIds[registerNumber];
	}

	public short[] getBytes() {
		return registerBytes;
	}

	public int getRegisterNumber() {
		return registerNumber;
	}

}
