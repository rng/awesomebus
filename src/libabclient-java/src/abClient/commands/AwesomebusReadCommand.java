package abClient.commands;

public class AwesomebusReadCommand extends AwesomebusCommand {

	private RegisterID registerID;

	public AwesomebusReadCommand(int nodeNumber, int registerNumber) {
		this(NodeID.getNodeID(nodeNumber), RegisterID
				.getRegisterID(registerNumber));
	}

	public AwesomebusReadCommand(NodeID nodeID, RegisterID registerID) {
		super(AwesomebusCommandType.READ, nodeID);
		this.registerID = registerID;
	}

	@Override
	public short[] getCommandArgumentBytes() {
		return registerID.getBytes();
	}
}
