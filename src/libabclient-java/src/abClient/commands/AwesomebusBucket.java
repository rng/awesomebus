package abClient.commands;

import java.util.ArrayList;

import abClient.util.ByteUtils;


public class AwesomebusBucket {

	private final ArrayList<AwesomebusCommand> awesomebusCommands = new ArrayList<AwesomebusCommand>();
	private NodeID nodeID;

	private static final int MAX_NUMBER_OF_COMMANDS_PER_BUCKET = 255;

	public AwesomebusBucket() {
	}

	public void addCommand(AwesomebusCommand awesomebusCommand) {
		if (awesomebusCommands.size() == MAX_NUMBER_OF_COMMANDS_PER_BUCKET) {
			throw new RuntimeException(
					"tried to add too many commands to a single bucket");
		}
		if (awesomebusCommands.size() == 0) {
			nodeID = awesomebusCommand.getNodeID();
		} else {
			if (awesomebusCommand.getNodeID() != nodeID) {
				throw new IllegalArgumentException("tried to add a command "
						+ awesomebusCommand + " with nodeID "
						+ awesomebusCommand.getNodeID()
						+ "to a bucket with nodeID " + nodeID);
			}
		}
		awesomebusCommands.add(awesomebusCommand);
	}

	public short[] getBytes() {
		if (awesomebusCommands.size() == 0) {
			throw new RuntimeException(
					"can't get the bytes of an empty bucket!");
		}

		ArrayList<short[]> allByteArrays = new ArrayList<short[]>();

		short numCommands = (short) awesomebusCommands.size();

		// first write the number of commands
		allByteArrays.add(new short[] { numCommands });
		// next write the node which we're targetting
		allByteArrays.add(nodeID.getBytes());

		// then the bytes associated with the commands
		for (AwesomebusCommand awesomebusCommand : awesomebusCommands) {
			allByteArrays.add(awesomebusCommand.getCommandBytes());
		}

		short[] combinedBytes = ByteUtils.combineShortArrays(allByteArrays);
		return combinedBytes;
	}
}
