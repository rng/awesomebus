package abClient.commands.responses;

import abClient.commands.AwesomebusCommandType;
import abClient.commands.NodeID;
import abClient.commands.RegisterID;

public class AwesomebusReadCommandResponse extends AwesomebusCommandResponse {

	protected RegisterID registerID;
	protected int valueRead;

	protected AwesomebusReadCommandResponse(NodeID nodeID) {
		super(AwesomebusCommandType.READ, nodeID);
	}

	public int getValueRead() {
		return valueRead;
	}

	public RegisterID getRegisterID() {
		return registerID;
	}

	public String toString() {
		String ret = "AwesomebusReadCommandResponse from node "
				+ getNodeID().getNodeNumber() + ", reg "
				+ registerID.getRegisterNumber() + " is "
				+ Integer.toHexString(valueRead);
		return ret;
	}

}
