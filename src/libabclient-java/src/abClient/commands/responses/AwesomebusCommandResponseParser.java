package abClient.commands.responses;

public class AwesomebusCommandResponseParser {

	private int newOffset;

	public AwesomebusCommandResponseParser() {
		newOffset = 0;
	}

	public AwesomebusCommandResponse parseCommand(short[] byteArrayToParse) {
		return parseCommand(byteArrayToParse, newOffset);
	}

	/**
	 * right now only need to worry about read responses
	 * 
	 * @param byteArrayToParse
	 * @param offset
	 * @return
	 */
	public AwesomebusCommandResponse parseCommand(short[] byteArrayToParse,
			int offset) {
		AwesomebusCommandResponse response = AwesomebusCommandResponse
				.parseFromBytes(byteArrayToParse, offset);
		if (response == null) {
			return null;
		}
		newOffset += response.getNumberOfBytesParsed();
		return response;
	}

	public int getNewOffset() {
		return newOffset;
	}

}
