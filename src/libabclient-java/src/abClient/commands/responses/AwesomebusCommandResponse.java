package abClient.commands.responses;

import abClient.commands.*;

public abstract class AwesomebusCommandResponse {

	protected final NodeID nodeID;
	private final AwesomebusCommandType awesomebusCommandType;

	protected AwesomebusCommandResponse(
			AwesomebusCommandType awesomebusCommandType, NodeID nodeID) {
		this.awesomebusCommandType = awesomebusCommandType;
		this.nodeID = nodeID;
	}

	public NodeID getNodeID() {
		return nodeID;
	}

	protected int numberOfBytesParsed;

	public int getNumberOfBytesParsed() {
		return numberOfBytesParsed;
	}

	public static AwesomebusCommandResponse parseFromBytes(short[] bytes,
			int offset) {

		// int commandNumber = (bytes[offset+1] >> 4) & 0XFF;
		// int commandNumber = (bytes[offset + 1]) & 0XF;
		int commandNumber = (bytes[offset + 1]) & 0XF;

		// System.out
		// .println("AwesomebusCommandResponse.parseFromBytes() bytes[offset + 1] = "
		// + bytes[offset + 1]);
		// System.out
		// .println("AwesomebusCommandResponse.parseFromBytes() commandNumber = "
		// + commandNumber);

		if (commandNumber == 0) {
			return null;
		}

		if (commandNumber == AwesomebusCommandType.READ
				.getByteIdentifierOfCommand()[0]) {

			AwesomebusReadCommandResponse readCommandResponse = new AwesomebusReadCommandResponse(
					NodeID.getNodeID(bytes[offset + 0]));

			short registerNumber = (short) (((bytes[offset + 1]) >> 4) & 0XF);
			readCommandResponse.registerID = RegisterID
					.getRegisterID(registerNumber);
			
			// int valueRead = ((bytes[offset + 2]) >> 24) & 0XFF;
			// valueRead += ((bytes[offset + 3]) >> 16) & 0XFF;
			// valueRead += ((bytes[offset + 4]) >> 8) & 0XFF;
			int valueRead = ((bytes[offset + 2]) << 16);
			valueRead += ((bytes[offset + 3]) << 8);
			valueRead += ((bytes[offset + 4]));

			readCommandResponse.valueRead = valueRead;

			readCommandResponse.numberOfBytesParsed = 6;

			if (readCommandResponse.nodeID == null) {
				throw new NullPointerException("node id is null");
			}

			return readCommandResponse;
		} else {
			throw new RuntimeException("response command type " + commandNumber
					+ " not supported yet");
		}
	}
}
