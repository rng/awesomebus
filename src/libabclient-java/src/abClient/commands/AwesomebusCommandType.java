package abClient.commands;

public enum AwesomebusCommandType {
	READ, WRITE, DHCP;

	private static final short READ_ID = 1;
	private static final short WRITE_ID = 2;
	private static final short DHCP_ID = 4;

	public short[] getByteIdentifierOfCommand() {
		short[] ret = new short[1];
		switch (this) {
		case READ:
			ret[0] = (short) READ_ID;
			break;
		case WRITE:
			ret[0] = (short) WRITE_ID;
			break;
		case DHCP:
			ret[0] = (short) DHCP_ID;
			break;
		}
		return ret;
	}

}
