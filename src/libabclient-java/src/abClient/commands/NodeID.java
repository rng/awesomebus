package abClient.commands;

public class NodeID {

	private int nodeNumber;
	private short[] nodeBytes;

	private static final int MAX_NODE_NUMBER = 255;
	private static NodeID[] nodeIds = new NodeID[MAX_NODE_NUMBER - 1];

	private NodeID(int nodeNumber) {
		this.nodeNumber = nodeNumber;
		nodeBytes = new short[1];
		nodeBytes[0] = (short) nodeNumber;
	}

	/**
	 * nodeNumbers are 1 based. Sorry.
	 */
	public static NodeID getNodeID(int nodeNumber) {
		if (nodeNumber < 1) {
			throw new IllegalArgumentException(
					"node number must be positive, was " + nodeNumber);
		}
		if (nodeNumber > MAX_NODE_NUMBER) {
			throw new IllegalArgumentException(
					"node number must not be larger than max node number ("
							+ MAX_NODE_NUMBER + "), was " + nodeNumber);
		}
		if (nodeIds[nodeNumber - 1] == null) {
			nodeIds[nodeNumber - 1] = new NodeID(nodeNumber);
		}
		return nodeIds[nodeNumber - 1];
	}

	public short[] getBytes() {
		return nodeBytes;
	}

	public int getNodeNumber() {
		return nodeNumber;
	}

}
