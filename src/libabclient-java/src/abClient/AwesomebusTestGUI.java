package abClient;

/**
 * GUI to interact with awesomebusd. Let's you display the nodes and their
 * register values, as well as write register values.
 * 
 * We want to work with an abstraction on top of the AwesomebusJavaInterface,
 * which is the low level interface to awesomed. We want a higher level
 * abstraction, say AwesomebusControlLoop. Has writeReg(node, reg, val)
 * 
 * @author jrebula
 * 
 */
public class AwesomebusTestGUI {

	public AwesomebusTestGUI() {
		SWTWindow swtWindow = new SWTWindow();
		swtWindow.runGUI("AwesomebusTestGUI");

	}

	public static void main(String[] args) {
		new AwesomebusTestGUI();
	}

}
