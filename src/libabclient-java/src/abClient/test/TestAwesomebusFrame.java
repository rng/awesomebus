package abClient.test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import abClient.commands.AwesomebusBucket;
import abClient.commands.AwesomebusCommand;
import abClient.commands.AwesomebusFrame;
import abClient.commands.AwesomebusReadCommand;
import abClient.commands.AwesomebusWriteCommand;


public class TestAwesomebusFrame {

	@Before
	public void setUp() throws Exception {
	}

	public static AwesomebusFrame getTestFrame() {
		AwesomebusFrame frame = new AwesomebusFrame();

		AwesomebusBucket awesomebusBucket = new AwesomebusBucket();

		AwesomebusCommand read1 = new AwesomebusReadCommand(1, 6);
		AwesomebusCommand write1 = new AwesomebusWriteCommand(1, 0, 3 << 16);
		AwesomebusCommand write2 = new AwesomebusWriteCommand(1, 6, (9 << 16)
				+ (0xA << 8) + (0XB));
		AwesomebusCommand read2 = new AwesomebusReadCommand(1, 0);

		awesomebusBucket.addCommand(read1);
		awesomebusBucket.addCommand(write1);
		awesomebusBucket.addCommand(write2);
		awesomebusBucket.addCommand(read2);

		frame.add(awesomebusBucket);

		return frame;
	}

	@Test
	public void testAddCommand() {
		AwesomebusCommand awesomebusCommand = new AwesomebusReadCommand(1, 0);
		AwesomebusFrame frame = new AwesomebusFrame();
		frame.addCommand(awesomebusCommand);

		// System.out.println("AwesomebusFrameTest.testAddCommand() " + frame);
	}

	// @Test
	// public void testCondenseCommandsIntoUniqueBuckets() {
	// fail("Not yet implemented");
	// }

	@Test
	public void testGetBytes() {
		int expectedNumberOfBytes = 13;
		short[] proofBytes = new short[expectedNumberOfBytes];
		for (int i = 0; i < expectedNumberOfBytes; i++) {
			proofBytes[i] = TestSimpleSharedMemory.bytesToWrite[i];
		}
		assertArrayEquals(proofBytes, getTestFrame().getBytes());
	}

	@Test
	public void testReadCommandBoundsBytes() {
		try {
			AwesomebusCommand readCommand = new AwesomebusReadCommand(1, 24);
			fail("shouldn't be able to create a read command for registers greater than 23");
		} catch (IllegalArgumentException e) {
			// ok
		}
	}

	public static AwesomebusFrame getSimpleReadFrame() {
		AwesomebusCommand awesomebusCommand = new AwesomebusReadCommand(1, 0);
		AwesomebusFrame frame = new AwesomebusFrame();
		frame.addCommand(awesomebusCommand);
		return frame;
	}

}
