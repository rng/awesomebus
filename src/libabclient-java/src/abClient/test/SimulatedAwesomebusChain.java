package abClient.test;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.Console;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

import abClient.util.SystemCommandExecutor;


public class SimulatedAwesomebusChain {

	private int numberOfNodes;
	private String interfaceFile;
	private String networkInterfaceName;

	private SystemCommandExecutor simulatorExecutor;
	private SystemCommandExecutor awesomedExecutor;

	public SimulatedAwesomebusChain(int numberOfNodes, String interfaceFile,
			String networkInterfaceName) {
		super();
		this.numberOfNodes = numberOfNodes;
		this.interfaceFile = interfaceFile;
		this.networkInterfaceName = networkInterfaceName;
	}

	public static SimulatedAwesomebusChain startDefaultSimulatedChain(
			int numberOfNodes, String interfaceFile) throws IOException,
			InterruptedException {
		String networkInterfaceName = SimulatedAwesomebusChain
				.getAUniqueishInterfaceName();

		SimulatedAwesomebusChain simulatedAwesomebusChain = new SimulatedAwesomebusChain(
				numberOfNodes, interfaceFile, networkInterfaceName);
		simulatedAwesomebusChain.startUpSimulatedChain();

		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		return simulatedAwesomebusChain;
	}

	private static String projectRootFolder() {
		String ret = "";

		File f = new File(".");
		ret = f.getAbsolutePath();
		ret += "/..";
		ret += "/..";

		// System.out
		// .println("SimulatedAwesomebusChain.projectRootFolder() f.getAbsolutePath() = "
		// + ret);

		return ret;
	}

	private void startARootCommand() throws IOException, InterruptedException {
		List<String> command = new ArrayList<String>();
		command.add("gksudo");
		command.add("ls");
		command.add(projectRootFolder());
		SystemCommandExecutor executor = new SystemCommandExecutor(command,
				true);
		executor.startExecutingCommand();
		executor.waitForProcessToEnd();
	}

	private void buildAllCode() throws IOException, InterruptedException {
		List<String> command = new ArrayList<String>();
		command.add("make");
		command.add("--directory=" + projectRootFolder());
		command.add("clean");
		SystemCommandExecutor executor = new SystemCommandExecutor(command,
				false);
		executor.startExecutingCommand();
		executor.waitForProcessToEnd();

		command.remove(command.size() - 1);
		executor = new SystemCommandExecutor(command, false);
		executor.startExecutingCommand();
		executor.waitForProcessToEnd();
	}

	// private static String redirectOutputToNull = " &";
	// //" > /dev/null 2> /dev/null"; //
	private boolean shouldGobbleStreams = true; // false; //

	private void startAwesomeSim(String interfaceName, int numNodes)
			throws IOException, InterruptedException {
		List<String> command = new ArrayList<String>();

		command.add("bash");
		command.add("runAwesomesim.sh");
		command.add(numNodes + "");
		command.add(interfaceName);

		// command.add("sudo");
		// command.add(projectRootFolder() + "/awesomesim/awesomesim.py");
		// command.add(numNodes + "");
		// command.add(interfaceName);
		// command.add("&");

		// command.add("> /dev/null 2> /dev/null");
		simulatorExecutor = new SystemCommandExecutor(command, true);
		simulatorExecutor.setShouldGobbleStreams(shouldGobbleStreams);
		simulatorExecutor.startExecutingCommand();
	}

	private void startAwesomed(String interfaceName, String interfaceFilename)
			throws IOException, InterruptedException {
		List<String> command = new ArrayList<String>();
		command.add("bash");
		command.add("runAwesomed.sh");
		command.add(interfaceName);
		command.add(interfaceFilename);

		// command.add("sudo");
		// command.add(projectRootFolder() + "/awesomed/awesomed");
		// command.add(interfaceName);
		// command.add(interfaceFilename);
		// command.add("&");

		// command.add("> /dev/null 2> /dev/null");
		awesomedExecutor = new SystemCommandExecutor(command, true);
		awesomedExecutor.setShouldGobbleStreams(shouldGobbleStreams);
		awesomedExecutor.startExecutingCommand();
	}

	public void killChain() {
		awesomedExecutor.killProcess();
		simulatorExecutor.killProcess();

		try {
			System.out.println("killChain: pid for awesomed: "
					+ awesomedExecutor.getFirstLineOutputted());
			killProcessNamed(awesomedExecutor.getFirstLineOutputted());
			killProcessNamed(simulatorExecutor.getFirstLineOutputted());
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

	}

	private void killProcessNamed(String pid) throws IOException,
			InterruptedException {
		pid = pid.replace(" ", "");

		List<String> command = new ArrayList<String>();
		//command.add("sudo");
		command.add("kill");
		command.add("-TERM");
		command.add(pid);

		SystemCommandExecutor executingExecutor = new SystemCommandExecutor(
				command, false);
		executingExecutor.startExecutingCommand();
		executingExecutor.waitForProcessToEnd();
	}

	public static String getAUniqueishInterfaceName() {
		int interfaceNumber = (int) (System.currentTimeMillis() % 1e5);
		return "awe" + interfaceNumber;
	}

	public static void main(String[] args) throws IOException,
			InterruptedException {

		int numberOfNodes = 4;
		String interfaceFile = "/dev/shm/awesomebus-shm";
		String networkInterfaceName = getAUniqueishInterfaceName();
		SimulatedAwesomebusChain simulatedAwesomebusChain = new SimulatedAwesomebusChain(
				numberOfNodes, interfaceFile, networkInterfaceName);

		simulatedAwesomebusChain.startUpSimulatedChain();
		Thread.sleep(1000L);
		System.out
				.println("SimulatedAwesomebusChain.main() finished running chain, killing...");
		simulatedAwesomebusChain.killChain();
		System.out.println("SimulatedAwesomebusChain.main() killed chain");
	}

	public void startUpSimulatedChain() throws IOException,
			InterruptedException {

		// buildAllCode();

		// startARootCommand();
		startAwesomeSim(networkInterfaceName, numberOfNodes);

		Thread.sleep(300);
		// System.err
		// .println("SimulatedAwesomebusChain.startUpSimulatedChain() started up awesomesim");

		startAwesomed(networkInterfaceName + "0", interfaceFile);
	}

	public int getNumberOfNodes() {
		return numberOfNodes;
	}

	public void setNumberOfNodes(int numberOfNodes) {
		this.numberOfNodes = numberOfNodes;
	}

	public String getInterfaceFile() {
		return interfaceFile;
	}

	public void setInterfaceFile(String interfaceFile) {
		this.interfaceFile = interfaceFile;
	}

	public String getNetworkInterfaceName() {
		return networkInterfaceName;
	}

	public void setNetworkInterfaceName(String networkInterfaceName) {
		this.networkInterfaceName = networkInterfaceName;
	}

}
