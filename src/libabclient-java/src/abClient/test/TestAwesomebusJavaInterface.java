package abClient.test;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.TimeoutException;

import org.junit.After;
import org.junit.Test;

import abClient.AwesomebusJavaInterface;
import abClient.commands.AwesomebusCommand;
import abClient.commands.AwesomebusFrame;
import abClient.commands.AwesomebusReadCommand;
import abClient.commands.responses.AwesomebusCommandResponse;
import abClient.commands.responses.AwesomebusReadCommandResponse;


public class TestAwesomebusJavaInterface {

	public static final String DEFAULT_SIMULATED_INTERFACE_FILE = AwesomebusJavaInterface.DEFAULT_SHARED_MEMORY_FILENAME;

	private SimulatedAwesomebusChain simulatedAwesomebusChain;

	@Test
	public void testAwesomebusJavaInterface() throws IOException,
			InterruptedException {
	    for (int numNodes = 1; numNodes < 5; numNodes++) {
		System.out.println("testAwesomebusJavaInterface numNodes = " + numNodes);
			testAwesomebusJavaInterface(numNodes);
			tearDown();
			//			Thread.sleep(200L);
								}
	}

	public void testAwesomebusJavaInterface(int numNodes) throws IOException,
			InterruptedException {

		// expected values:
		ArrayList<HashMap<Integer, Integer>> expectedValues = new ArrayList<HashMap<Integer, Integer>>();
		for (int i = 0; i < 5; i++) {
			expectedValues.add(new HashMap<Integer, Integer>());
			expectedValues.get(i).put(0, 3 << 16);
			expectedValues.get(i).put(6, (9 << 16) + (0xA << 8) + (0XB));
		}

		expectedValues.get(0).put(0, 0);
		expectedValues.get(0).put(6, 0);

		expectedValues.get(1).put(6, 0);

		AwesomebusJavaInterface awesomebusJavaInterface = new AwesomebusJavaInterface(
				DEFAULT_SIMULATED_INTERFACE_FILE, 128);
		awesomebusJavaInterface.initialize();

		SimulatedAwesomebusChain simulatedAwesomebusChain = SimulatedAwesomebusChain
				.startDefaultSimulatedChain(numNodes,
						DEFAULT_SIMULATED_INTERFACE_FILE);

		// test:
		for (int i = 0; i < expectedValues.size(); i++) {
			testResultsFromRoundTrip(awesomebusJavaInterface,
					expectedValues.get(i));
		}

		simulatedAwesomebusChain.killChain();
	}

    @Test
	public void testAwesomebusJavaInterfaceNodeEnumeration()
			throws IOException, InterruptedException {
		for (int nodeNum = 10; nodeNum <= 40; nodeNum += 10) {
			testAwesomebusJavaInterfaceNodeEnumerationForNodeNum(nodeNum);
		}
	}

	public void testAwesomebusJavaInterfaceNodeEnumerationForNodeNum(int nodeNum)
			throws IOException, InterruptedException {

		// expected values:
		ArrayList<Integer> expectedValues = new ArrayList<Integer>();
		for (int i = 0; i < nodeNum; i++) {
			expectedValues.add(i + 1);
		}

		for (int i = 0; i < 10; i++) {
			expectedValues.add(nodeNum);
		}

		AwesomebusJavaInterface awesomebusJavaInterface = new AwesomebusJavaInterface(
				DEFAULT_SIMULATED_INTERFACE_FILE, 128);
		awesomebusJavaInterface.initialize();

		SimulatedAwesomebusChain simulatedAwesomebusChain = SimulatedAwesomebusChain
				.startDefaultSimulatedChain(nodeNum,
						DEFAULT_SIMULATED_INTERFACE_FILE);

		// test:
		for (int i = 0; i < expectedValues.size(); i++) {
			testNodeNumberForRoundTrip(awesomebusJavaInterface,
					expectedValues.get(i));
		}

		simulatedAwesomebusChain.killChain();
	}

	private ArrayList<AwesomebusCommandResponse> getResponsesFromDefaultFrame(
			AwesomebusJavaInterface awesomebusJavaInterface) throws IOException {

		long timeoutMS = 1000L;
		AwesomebusFrame frame = TestAwesomebusFrame.getTestFrame();

		awesomebusJavaInterface.setAwesomebusFrame(frame);
		awesomebusJavaInterface.sendCommandsToBus();

		try {
			awesomebusJavaInterface.waitForReplyFromBus(timeoutMS);
		} catch (TimeoutException e) {
			fail("test apparently couldn't run because awesomed is not responding within timeout limits?");
		}

		if (awesomebusJavaInterface.isWaitingForReply()) {
			fail("programming error");
		}

		ArrayList<AwesomebusCommandResponse> responses = awesomebusJavaInterface
				.getCommandResults();
		assertEquals(2, responses.size());

		return responses;
	}

	public void testNodeNumberForRoundTrip(
			AwesomebusJavaInterface awesomebusJavaInterface,
			int numberOfNodesRequired) throws IOException {

		ArrayList<AwesomebusCommandResponse> responses = getResponsesFromDefaultFrame(awesomebusJavaInterface);

		assertEquals(numberOfNodesRequired,
				awesomebusJavaInterface.getNumberOfNodesFound());
	}

	@After
	public void tearDown() {
		File f = new File(DEFAULT_SIMULATED_INTERFACE_FILE);
		f.delete();
	}

	public void testResultsFromRoundTrip(
			AwesomebusJavaInterface awesomebusJavaInterface,
			HashMap<Integer, Integer> responseValuesRequired)
			throws IOException {
		ArrayList<AwesomebusCommandResponse> responses = getResponsesFromDefaultFrame(awesomebusJavaInterface);

		assertEquals(2, responses.size());

		for (AwesomebusCommandResponse response : responses) {
			AwesomebusReadCommandResponse readResponse = ((AwesomebusReadCommandResponse) response);

			int requiredResponse = responseValuesRequired.get(readResponse
					.getRegisterID().getRegisterNumber());

			System.out.println("testResultsFromRoundTrip: required: " + requiredResponse + ", got " + readResponse.getValueRead());

			assertEquals(requiredResponse, readResponse.getValueRead());
			// System.out
			// .println("TestAwesomebusJavaInterface.testAwesomebusJavaInterface() readResponse = "
			// + readResponse);
		}
	}

}
