package abClient.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import abClient.util.test.TestByteUtils;


@RunWith(value = Suite.class)
@SuiteClasses(value = { TestAwesomebusFrame.class,
		TestAwesomebusJavaInterface.class, TestByteUtils.class })
public class AllSharedMemoryJavaClientTests {

}
