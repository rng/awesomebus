package abClient.test;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.FileChannel.MapMode;
import java.util.ArrayList;

/**
 * run this using
 * 
 * -XX:+UnlockExperimentalVMOptions -XX:+UseG1GC -XX:MaxGCPauseMillis=1
 * -Xmx1200m -XX:GCPauseIntervalMillis=20 -Xmn1m
 */
public class TestSimpleSharedMemory {

	private static final String sharedMemoryDirectoryName = "/dev/shm/";

	private static final int numIterationsToTest = 3; // 100000; // 1000000; //
														// 100000;

	private static final int numIterationsForWarmup = numIterationsToTest / 5;

	private static final byte CMD_READ = 1;
	private static final byte CMD_WRITE = 2;

	private static final int MAX_BUFFER_SIZE = 50; // 800; // 1024; // 8192;

	private static int numBytesInPacket = MAX_BUFFER_SIZE; // 1000; // 20;
	public static byte[] bytesToWrite = new byte[numBytesInPacket];
	static {
		byte numBuckets = 1;
		byte numCommands = 4;
		byte nodeNumber = 1;

		int byteCounter = 0;
		bytesToWrite[byteCounter++] = numBuckets;
		bytesToWrite[byteCounter++] = numCommands;
		bytesToWrite[byteCounter++] = nodeNumber;

		bytesToWrite[byteCounter++] = CMD_READ + (6 << 4);

		// bytesToWrite[byteCounter++] = 6;

		// we have 24bit registers, so we need to write 3 bytes per register
		bytesToWrite[byteCounter++] = CMD_WRITE + (0 << 4);
		// bytesToWrite[byteCounter++] = 0;

		bytesToWrite[byteCounter++] = 3;
		bytesToWrite[byteCounter++] = 0;
		bytesToWrite[byteCounter++] = 0;

		// bytesToWrite[byteCounter++] = CMD_READ;
		// bytesToWrite[byteCounter++] = 0;
		bytesToWrite[byteCounter++] = CMD_WRITE + (6 << 4);
		// bytesToWrite[byteCounter++] = 6;

		bytesToWrite[byteCounter++] = 0x9;
		bytesToWrite[byteCounter++] = 0xA;
		bytesToWrite[byteCounter++] = 0xB;

		bytesToWrite[byteCounter++] = CMD_READ + (0 << 4);
		// bytesToWrite[byteCounter++] = 0;
	}

	public static void main(String[] args) throws IOException {

		File f = new File(sharedMemoryDirectoryName);
		f.mkdir();

		RandomAccessFile interfaceFile = new RandomAccessFile(
				sharedMemoryDirectoryName + "awesomebus-shm", "rw");

		interfaceFile.seek(0);
		interfaceFile.write(0);
		interfaceFile.write(bytesToWrite);
		interfaceFile.write(0);

		MappedByteBuffer mappedByteBuffer = interfaceFile.getChannel().map(
				MapMode.READ_WRITE, 0, MAX_BUFFER_SIZE + 2);

		// byte[] byteBuffer = new byte[100];
		// ArrayList<Byte> readInData = new ArrayList<Byte>();

		File directory = new File(sharedMemoryDirectoryName);
		directory.mkdirs();

		byte[] bytesToRead = new byte[numBytesInPacket];

		long[] loopTimes = new long[numIterationsToTest];
		long totalTime = 0;

		long[] waitTimes = new long[numIterationsToTest];
		long totalWaitTime = 0;
		int maxWaitTimeIndex = -1;
		long maxWaitTime = -1;

		long number = System.nanoTime();

		byte[] triggerBytes = new byte[1];

		long thisTime = 0;
		long lastTime = System.nanoTime();
		long maxTime = 0;
		int maxTimeIndex = -1;
		long startTimeWaiting = 0;
		double a = 0.0;

		for (int i = 0; i < numIterationsToTest; i++) {

			if (true) {
				// System.out.println("TestSimpleSharedMemory.main(), starting a new packet...");
				boolean useMappedByteBufferForWriting = false; // true; //
				boolean useMappedByteBufferForCheckingTrigger = true;// false;
																		// //
				boolean useMappedByteBufferForReading = true; // false; //

				triggerBytes[0] = 0;
				if (useMappedByteBufferForWriting) {
					mappedByteBuffer.rewind();
					mappedByteBuffer.put(0, triggerBytes[0]);
					mappedByteBuffer.put(bytesToWrite, 1,
							bytesToWrite.length - 1);
					triggerBytes[0] = 1;
					mappedByteBuffer.put(0, triggerBytes[0]);
					mappedByteBuffer.force();
				} else {
					interfaceFile.seek(0);
					interfaceFile.write(triggerBytes);
					interfaceFile.write(bytesToWrite);
					interfaceFile.seek(0);
					triggerBytes[0] = 1;
					interfaceFile.write(triggerBytes);
					interfaceFile.getChannel().force(true);
				}

				// now we play the waiting game. The master code is sending the
				// packet over the wire, when it is done, it will create a
				// packetRecievedFile signalling that we can load in the
				// returned packet data.
				startTimeWaiting = System.nanoTime();

				// System.out
				// .println("TestSimpleSharedMemory.main() waiting for response");
				while (triggerBytes[0] == 1) {
					if (useMappedByteBufferForCheckingTrigger) {
						mappedByteBuffer.load();
						triggerBytes[0] = mappedByteBuffer.get(0);
					} else {
						// interfaceFile.close();
						// interfaceFile = new RandomAccessFile(
						// sharedMemoryDirectoryName + "awesomebus-shm",
						// "rw");

						interfaceFile.seek(0);
						interfaceFile.getChannel().force(true);
						interfaceFile.read(triggerBytes);
					}

					// Thread.yield();
					// System.out
					// .println("TestSimpleSharedMemory.main() checking trigger "
					// + triggerBytes[0]);
				}

				// long byteLengthOfInput = interfaceFile.length();

				if (useMappedByteBufferForReading) {
					mappedByteBuffer.rewind();
					mappedByteBuffer.get();
					mappedByteBuffer
							.get(bytesToRead, 0, bytesToRead.length - 1);
				} else {
					interfaceFile.seek(1L);
					interfaceFile.read(bytesToRead);
				}

				System.out.print("response packet: ");
				for (byte b : bytesToRead) {
					System.out.print(b + ", ");
				}
				System.out.println();

				waitTimes[i] = System.nanoTime() - startTimeWaiting;
				totalWaitTime += waitTimes[i];

				if (waitTimes[i] > maxWaitTime && i > numIterationsForWarmup) {
					maxWaitTimeIndex = i;
					maxWaitTime = waitTimes[i];
				}
			}

			thisTime = System.nanoTime();
			loopTimes[i] = thisTime - lastTime;
			lastTime = thisTime;

			if (loopTimes[i] > maxTime && i > numIterationsForWarmup) {
				maxTimeIndex = i;
				maxTime = loopTimes[i];
			}

			// default garbage collector 0.05 worst loop time
			// -XX:+UseConcMarkSweepGC : 0.019 worst loop time
			// -XX:+CMSParallelRemarkEnabled : adding to the above increases to
			// 0.025
			// -XX:+UseParallelGC : 0.05 worst loop time
			// -XX:NewRatio=1 : no big effect?
			// -XX:+UseNUMA no big effect
			// -XX:+UnlockExperimentalVMOptions -XX:+UseG1GC
			// -XX:MaxGCPauseMillis=1// 0.014

			// maxTime = Math.max(maxTime, loopTimes[i]);

			totalTime += loopTimes[i];
			// and then we wait for the next frame

			// System.gc();
		}

		double meanTime = totalTime / ((double) numIterationsToTest);
		double meanWaitTime = totalWaitTime / ((double) numIterationsToTest);

		double totalVariance = 0.0;
		for (int i = 0; i < numIterationsToTest; i++) {
			totalVariance += Math.pow(loopTimes[i], 2) - Math.pow(meanTime, 2);
		}

		double standardDeviationLoopTime = Math.sqrt(totalVariance
				/ numIterationsToTest);

		System.out.println("TestSimpleSharedMemory.main() mean loop time = "
				+ meanTime / 1e9);
		System.out
				.println("TestSimpleSharedMemory.main() standard deviation of loop time = "
						+ standardDeviationLoopTime / 1e9);
		System.out.println("TestSimpleSharedMemory.main() maxTime = " + maxTime
				/ 1e9 + ", which occurs at index " + maxTimeIndex);
		System.out
				.println("TestSimpleSharedMemory.main() wait time at max time index was "
						+ waitTimes[maxTimeIndex] / 1e9);
		System.out.println("TestSimpleSharedMemory.main() maxWaitTime = "
				+ maxWaitTime / 1e9 + ", which occurs at index "
				+ maxWaitTimeIndex);

		// System.out.println("TestSimpleSharedMemory.main() a = " + a);
	}

}
