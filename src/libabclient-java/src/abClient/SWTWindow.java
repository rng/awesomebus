package abClient;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

public class SWTWindow {

	private Display display;
	private Shell topLevelShell;
	private Composite composite;

	public SWTWindow() {

		display = new Display();

		topLevelShell = new Shell(display);
		topLevelShell.setLayout(new GridLayout());

		composite = new Composite(topLevelShell, SWT.NONE);
		composite.setLayout(new FillLayout());
		composite.setLayoutData(new GridData(GridData.FILL, GridData.FILL,
				true, true, -1, -1));
	}

	public void startLoopingAsynchronously(final Runnable runnable) {
		getDisplay().asyncExec(new Runnable() {
			public void run() {
				runnable.run();
				getDisplay().asyncExec(this);
			}
		});
	}

	public void runGUI(String title) {
		topLevelShell.setText(title);
		topLevelShell.open();
		while (!topLevelShell.isDisposed()) {
			if (!display.readAndDispatch())
				display.sleep();
		}
		display.dispose();
	}

	public Display getDisplay() {
		return display;
	}

	public Shell getTopLevelShell() {
		return topLevelShell;
	}

	public Composite getComposite() {
		return composite;
	}

}
