/* vim:set ts=2 sw=2 sts=2 et: */
#include <abclient.h>
#include <stdio.h>
#include <stdint.h>
#include <math.h>
#include <unistd.h>
#include <time.h>
#include <assert.h>
#include <sched.h>
#include <stdlib.h>
#include <sys/utsname.h>


typedef struct {
  int32_t min;
  int32_t max;
  int64_t sum;
} MMA;


int max(int a, int b) {
  return a > b ? a : b;
}


int min(int a, int b) {
  return a > b ? b : a;
}


void mma_update(MMA *m, int v) {
  m->max = max(m->max, v);
  m->min = min(m->min, v);
  m->sum += v;
}


void stat_write_i(FILE *stats, char *name, int val, int sep) {
  fprintf(stats,"\"%s\":%d%s", name, val, sep ? "," : "");
}


void stat_write_s(FILE *stats, char *name, char *val, int sep) {
  fprintf(stats,"\"%s\":\"%s\"%s", name, val, sep ? "," : "");
}


struct timespec diff(struct timespec start, struct timespec end) {
	struct timespec temp;
	if ((end.tv_nsec-start.tv_nsec)<0) {
		temp.tv_sec = end.tv_sec-start.tv_sec-1;
		temp.tv_nsec = 1000000000+end.tv_nsec-start.tv_nsec;
	} else {
		temp.tv_sec = end.tv_sec-start.tv_sec;
		temp.tv_nsec = end.tv_nsec-start.tv_nsec;
	}
	return temp;
}


int test_ab_rw(FILE *stats, int iterations, int tickus) {
  ABClient abc;
  int w0, w1, r0, r1;
	struct timespec t0, t1, t2;
  MMA proctime = {1000000,0,0};

  abclient_open(&abc, "/awesomed");

  clock_gettime(CLOCK_REALTIME, &t0);

  for (int i=0;i<iterations;i++) {
    clock_gettime(CLOCK_REALTIME, &t1);
    w0 = i;
    w1 = i*2;
    abclient_read(&abc, 1, 10);
    abclient_read(&abc, 1, 11);
    abclient_write(&abc, 1, 10, w0);
    abclient_write(&abc, 1, 11, w1);
    // transfer
    int r = abclient_transfer(&abc);
    // parse response
    r0 = r1 = -1;
    for (int i=0;i<r;i++) {
      if (abc.packets[i].cmd == AD_VALUE) {
        uint32_t value = abc.packets[i].data.v.value;
        switch (abc.packets[i].data.v.reg) {
          case 10: r0 = value; break;
          case 11: r1 = value; break;
        }
      }
    }
    if (i > 0) {
      if (w0 != (r0+1)) {
        printf("failed reg0 mismatch %d<>%d\n", w0, r0);
        goto fail;
      }
      if (w1 != (r1+2)) {
        printf("failed reg1 mismatch %d<>%d\n", w1, r1);
        goto fail;
      }
    }
    
    clock_gettime(CLOCK_REALTIME, &t2);
		// calculate timing
		struct timespec tdiff = diff(t1, t2);
		assert((long)tdiff.tv_sec == 0);
    int proctimeus = tdiff.tv_nsec / 1000;
    mma_update(&proctime, proctimeus);
		// try to keep cycle time correct
		if (proctimeus < tickus) {
			usleep(tickus - proctimeus);
		}
  }
  
  clock_gettime(CLOCK_REALTIME, &t1);

  struct timespec tdiff = diff(t0, t1);
  int totalus = tdiff.tv_sec*1000000 + (tdiff.tv_nsec/1000);

  stat_write_i(stats, "proctimeavg", (int32_t)(proctime.sum/iterations), 1);
  stat_write_i(stats, "proctimemin", proctime.min, 1);
  stat_write_i(stats, "proctimemax", proctime.max, 1);
  stat_write_i(stats, "ticktimetotal", totalus, 1);
  stat_write_i(stats, "ticktimeavg", totalus/iterations, 0);

  abclient_close(&abc);
  return 0;
fail:
  abclient_close(&abc);
  return 1;
}


int test_ab_rw_std(FILE *stats, int iterations) {
  return test_ab_rw(stats, iterations, 1000);
}


int test_ab_rw_nolimit(FILE *stats, int iterations) {
  return test_ab_rw(stats, iterations, 0);
}


int test_ab_rw_sched(FILE *stats, int iterations) {
	struct sched_param sp, osp;
  int opolicy;

  // save old scheduler
  opolicy = sched_getscheduler(0);
  sched_getparam(0, &osp);
	// set FIFO scheduler with high priority
	if (opolicy == -1) {
		perror("couldn\"t get scheduler");
		exit(-1);
	}
	sp.sched_priority = sched_get_priority_max(SCHED_FIFO);
	sched_setscheduler(0, SCHED_FIFO, &sp);
	
  int r = test_ab_rw(stats, iterations, 1000);
  
  // restore old scheduler
  sched_setscheduler(0, opolicy, &osp);
  return r;
}


int main(int argc, char *argv[]) {
  struct Benchmark {
    char *name;
    int (*func)(FILE *stats, int iterations);
  } benchmarks[] = {
    {"RW Test (std)", test_ab_rw_std},
    {"RW Test (no limit)", test_ab_rw_nolimit},
    {"RW Test (rt sched)", test_ab_rw_sched},
    {0},
  };

  if (argc != 2) {
    printf("usage: ./abbench <iterations>\n");
    exit(1);
  }
  int iterations = atoi(argv[1]);

  printf("ABBench starting (%d iterations)\n", iterations);

  // create stats file
  FILE *stats = fopen("stats.json", "wb");
  fprintf(stats, "{");

  struct utsname unamedata;
  uname(&unamedata);

  // save uname data
  fprintf(stats, "\"uname\":{");
  stat_write_s(stats, "nodename", unamedata.nodename, 1);
  stat_write_s(stats, "release", unamedata.release, 1);
  stat_write_s(stats, "version", unamedata.version, 1);
  stat_write_s(stats, "machine", unamedata.machine, 0);
  fprintf(stats, "},");

  // save iteration count
  stat_write_i(stats, "iterations", iterations, 1);

  // run and save benchmark data
  fprintf(stats, "\"benchmarks\":[");
  int i = 0;
  while (benchmarks[i].name) {
    // save benchmark name
    fprintf(stats, (i>0) ? ",{" : "{");
    stat_write_s(stats, "name", benchmarks[i].name, 1);
    fprintf(stats, "\"stats\":{");
    // run benchmark
    printf("Running %-20s: ", benchmarks[i].name);
    int r = benchmarks[i].func(stats, iterations);
    printf("%s\n", r ? "FAILED" : "PASSED");
    // save benchmark results
    fprintf(stats, "},");
    stat_write_i(stats, "result", r, 0);
    fprintf(stats, "}");
    i++;
  }

  // close stats file
  fprintf(stats, "]");
  fprintf(stats, "}");
  fclose(stats);
  
  printf("ABBench complete\n");

  return 0;
}
