#!/usr/bin/env python
# vim:set ts=2 sw=2 sts=2 et:
import json

stats = json.load(file("stats.json", "rb"))

print "Test Machine:"
print " Name:   %s" % (stats["uname"]["nodename"])
print " Kernel: %s (%s)" % (stats["uname"]["release"], stats["uname"]["version"])
print " Arch:   %s" % (stats["uname"]["machine"])
print ""
print "Config:"
print " Iterations: %d" % (stats["iterations"])
print ""
print "Benchmarks:"
for b in stats["benchmarks"]:
  print " %s [%s]:" % (b["name"], ("FAILED" if b["result"] else "PASSED"))
  for sn,sv in sorted(b["stats"].items()):
    print "  %-15s: %s" % (sn, sv)
  print ""
