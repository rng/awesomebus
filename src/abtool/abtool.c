/* vim:set ts=2 sw=2 sts=2 et: */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <arpa/inet.h>
#include "awesomebus.h"

uint32_t buckets[8];
uint32_t len;
uint8_t tpkt[1024];
uint8_t rpkt[1024];
ABFrame *tframe, *rframe;


// Check returned opcode, if it's not a NOP then we have a response
bool check_if_handled(int bucket, uint32_t *intval) {
  ABBucket *bkt = (ABBucket*)(((char*)rframe) + buckets[bucket]);
  int retword = ntohl(bkt->instructions[0]);
  if (intval)
    *intval = retword;
  return ((retword & 0xF) == AB_OP_NOP);
}


// Dump all registers of a node
int command_dump(ABInterface *abif, char *argv[]) {
  int node = atoi(argv[3]);
  int r;

  ab_frame_create(tframe);
  AB_ADD_BUCKET(tframe, node, 
    AB_READ(0),  AB_READ(1),  AB_READ(2),  AB_READ(3),
    AB_READ(4),  AB_READ(5),  AB_READ(6),  AB_READ(7),
    AB_READ(8),  AB_READ(9),  AB_READ(10), AB_READ(11),
    AB_READ(12), AB_READ(13), AB_READ(14), AB_READ(15),
  );
  ab_frame_finish(tframe, &len, buckets);

  if ((r=ab_interface_process(abif, tpkt, rpkt, len)) == AB_ERR_OK) {
    ab_frame_decode(tframe, rframe, buckets, 1);
  } else {
    printf("Error processing packet: %s\n", ab_error_string(r));
  }
  return 0;
}


// Read a node register
int command_read(ABInterface *abif, char *argv[]) {
  int node = atoi(argv[3]);
  int reg = atoi(argv[4]);
  int r;

  ab_frame_create(tframe);
  AB_ADD_BUCKET(tframe, node, AB_READ(reg));
  ab_frame_finish(tframe, &len, buckets);

  if ((r=ab_interface_process(abif, tpkt, rpkt, len)) == AB_ERR_OK) {
    ABBucket *bkt = (ABBucket*)(((char*)rframe) + buckets[0]);
    int retword = ntohl(bkt->instructions[0]);
    printf("Read  r%02d @ n%02d = %06x\n", reg, node, retword>>8);
  } else {
    printf("Error processing packet: %s\n", ab_error_string(r));
  }
  return 0;
}


// Write a node register
int command_write(ABInterface *abif, char *argv[]) {
  int node = atoi(argv[3]);
  int reg = atoi(argv[4]);
  uint32_t value = atoi(argv[5]);
  int r;

  ab_frame_create(tframe);
  AB_ADD_BUCKET(tframe, node, AB_WRITE(reg, value));
  ab_frame_finish(tframe, &len, buckets);

  if ((r=ab_interface_process(abif, tpkt, rpkt, len)) == AB_ERR_OK) {
    printf("Wrote r%02d @ n%02d = %06x\n", reg, node, value);
  } else {
    printf("Error processing packet: %s\n", ab_error_string(r));
  }
  return 0;
}


// Allocate an address to the first unallocated node
int command_dhcp(ABInterface *abif, char *argv[]) {
  int node = 0xFF;
  int nodeNumber = atoi(argv[3]);
  int r;

  ab_frame_create(tframe);
  AB_ADD_BUCKET(tframe, node, AB_SETADDR(nodeNumber));
  ab_frame_finish(tframe, &len, buckets);
  
  if ((r=ab_interface_process(abif, tpkt, rpkt, len)) == AB_ERR_OK) {
    if (check_if_handled(0, NULL)) {
      printf("dhcp set a new node address\n");
    } else {
      printf("No new node from dhcp command\n");
    }
  } else {
    printf("Error processing packet: %s\n", ab_error_string(r));
  }
  return 0;
}


// Enumerate all nodes on the bus
int command_enum(ABInterface *abif, char *argv[]) {
  // Enumerate nodes by attempting a read of each node's r0
  for (int i=0; i<32; i+=8) {
    // batch in groups of 8 buckets 
    // (I think we could do it with a single frame)
    ab_frame_create(tframe);
    for (int node=0; node<8; node++) {
      AB_ADD_BUCKET(tframe, i+node, AB_READ(0));
    }
    ab_frame_finish(tframe, &len, buckets);
    ab_interface_process(abif, tpkt, rpkt, len);
    for (int node=0; node<8; node++) {
      if (check_if_handled(node, NULL)) {
        printf("Node at %i\n", i+node);
      }
    }
  }
  return 0;
}


// Read node config value
int command_confread(ABInterface *abif, char *argv[]) {
  int node = atoi(argv[3]);
  int ofs = atoi(argv[4]);

  ab_frame_create(tframe);
  AB_ADD_BUCKET(tframe, node, AB_CONFIGREAD(ofs));
  ab_frame_finish(tframe, &len, buckets);
  ab_interface_process(abif, tpkt, rpkt, len);

  uint32_t retword = 0;
  if (check_if_handled(0, &retword)) {
    printf("confread %d @ n%d = %04x\n", ofs, node, retword>>16);
  } else {
    printf("No node responding at address %d\n", node);
  }
  return 0;
}


// Write node config value
int command_confwrite(ABInterface *abif, char *argv[]) {
  int node = atoi(argv[3]);
  int ofs = atoi(argv[4]);
  int value = atoi(argv[5]);

  ab_frame_create(tframe);
  AB_ADD_BUCKET(tframe, node, AB_CONFIGWRITE(ofs, value));
  ab_frame_finish(tframe, &len, buckets);
  ab_interface_process(abif, tpkt, rpkt, len);

  if (check_if_handled(0, NULL)) {
    printf("confwrite %d @ n%d = %04x\n", ofs, node, value);
  } else {
    printf("No node responding at address %d\n", node);
  }
  return 0;
}


struct Command {
  char *name;
  int argcount;
  int (*func)(ABInterface *abif, char *argv[]);
  char *help;
} commands[] = {
  { "enum",   0, command_enum,      "" },
  { "dhcp",   1, command_dhcp,      "<address>" },
  { "dump",   1, command_dump,      "<node>" },
  { "read",   2, command_read,      "<node> <reg>" },
  { "write",  3, command_write,     "<node> <reg> <value>" },
  { "confrd", 2, command_confread,  "<node> <offset>" },
  { "confwr", 3, command_confwrite, "<node> <offset> <value>" },
  { NULL }
};


void usage() {
  printf("usage: ./abtool <interface> <command> [args]\n");
  printf("commands:\n");
  int i = 0;
  struct Command c;
  while ((c=commands[i++]).name) {
    printf(" %6s %s\n", c.name, c.help);
  }
  exit(1);
}


int main(int argc, char *argv[]) {

  if (argc<3) usage();
  ABInterface abif;

  // create socket
  if (ab_interface_open(&abif, argv[1]) != AB_ERR_OK) {
    printf("Couldn't open awesomebus interface\n");
    exit(1);
  }
  
  char *cmd = argv[2];

  // create packet
  memset(tpkt, 0, sizeof(tpkt));
  memset(rpkt, 0, sizeof(rpkt));
  tframe = (ABFrame*)tpkt;
  rframe = (ABFrame*)rpkt;

  int i = 0;
  struct Command c;
  bool ok = false;
  while ((c=commands[i++]).name) {
    if (strcmp(c.name, cmd) == 0) {
      if (argc == (3 + c.argcount)) {
        c.func(&abif, argv);
        ok = true;
      } else {
        printf("Invalid arguments for command '%s'\n", cmd);
        usage();
      }
    }
  }

  if (!ok) {
    printf("Unknown command '%s'\n", cmd);
    usage();
  }

  ab_interface_close(&abif);

  return 0;
}
