#include <Python.h>
#include <stdio.h>

int main(int argc, char *argv[]) {
    char pyfile[256];
    snprintf(pyfile, 256, "%s.py", argv[0]);
    FILE *f = fopen(pyfile, "rb");

    Py_Initialize();
    PySys_SetArgv(argc, argv);
    PyRun_SimpleFile(f, "awesomesim.py");
    Py_Finalize();
    fclose(f);
}
