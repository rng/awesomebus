#! /usr/bin/env python
# vim:set ts=2 sw=2 sts=2 et:

# Awesomebus network simulator
# 
# Creates a TUN/TAP virtual ethernet device and creates a simulated
# awesomebus network with a number of nodes.
#
# Usage:
# > sudo ./awesomesim.py 4 awesome
# > sudo ./abtool awesome0 dhcp 1
# > sudo ./abtool awesome0 write 1 1 1234
# > sudo ./abtool awesome0 read 1 1

import os, sys
from fcntl import ioctl
from select import select
import argparse
import socket
import ctypes
import struct
import array
import logging

SIOCGIFFLAGS = 0x8913
SIOCSIFFLAGS = 0x8914
TUNSETIFF    = 0x400454ca
IFF_TUN      = 0x0001
IFF_TAP      = 0x0002
IFF_UP       = 1

class ifreq(ctypes.Structure):
  _fields_ = [("ifr_ifrn", ctypes.c_char * 16),
              ("ifr_flags", ctypes.c_short)]


# Awesomebus instruction opcodes
(
 AB_OP_NOP,
 AB_OP_READ,
 AB_OP_WRITE,
 AB_OP_PING,
 AB_OP_SETADDR,
 AB_OP_CONFIG,
) = range(6)


# Basic node simulator
class VirtualNode:
  def __init__(self):
    self.registers = [0] * 256
    self.addr = 0xFF

  def process_instruction(self, addr, inst):
    if self.addr != addr:
      return inst
    op = inst&0xF
    #print "op is %d" % op
    reg = (inst>>4)&0xF
    val = (inst>>8)
    #print "inst&0xFF is %X" % (inst&0xFF)
    if op == AB_OP_NOP:
      pass
    elif op == AB_OP_READ:
      return ((self.registers[reg]<<8) + (inst&0xFF))
    elif op == AB_OP_WRITE:
      self.registers[reg] = val
    elif op == AB_OP_PING:
      if self.addr != 0xFF:
        return AB_OP_NOP
    elif op == AB_OP_SETADDR:
      if self.addr == 0xFF:
        logging.info("%s: setting address to %d" % (self, val))
        self.addr = val
        return AB_OP_NOP
    elif op == AB_OP_CONFIG:
      isread = ((inst>>4)&1) == 1
      # config registers come after standard regs
      # (need to take 16b/32b into account on xmos) 
      ofs = ((inst>>5)&0x7FF) + 16
      if isread:
        return self.registers[ofs]<<16
      else:
        self.registers[ofs] = inst>>16
    else:
      logging.error("invalid instruction 0x%08x" % inst)
    return inst

  def __repr__(self):
    return "<VirtualNode %04X (0x%02X)>" % (id(self)&0xFFFF, self.addr)


# Process an ethernet frame through all the virtual nodes
def process_frame(nodes, pkt):
  logging.info("-- Processing frame (%d bytes)"  % len(pkt))
  if len(pkt) < 20:
    logging.error("bad packet length: %d" % len(pkt))
    return

  pktfmt = "!HH6B6BHH"
  hdr = struct.unpack(pktfmt, pkt[:20])
  frommac,tomac,ethertype,wordlen = hdr[2:8], hdr[8:14], hdr[14], hdr[15]

  if ethertype != 0x8888:
    logging.error("invalid ethertype: 0x%04X" % ethertype)
    return
  
  if (20 + (wordlen*4)) != len(pkt):
    logging.error("invalid packet size: %d" % wordlen)
    return

  # process a frame, running each bucket of instructions through all the nodes
  for node in nodes:
    ptr = 20
    while ptr < len(pkt):
      nodeaddr, instcount = struct.unpack_from("!HH", pkt, ptr)
      insts = struct.unpack_from("!%dI" % instcount, pkt, ptr+4)
      for iptr, inst in enumerate(insts):
        resp = node.process_instruction(nodeaddr, inst)
        struct.pack_into("!I", pkt, ptr + 4 + iptr*4, resp)
      ptr += (instcount+1) * 4

  return pkt


# Run simulator
def main(args):

  parser = argparse.ArgumentParser(description="Create a virtual Awesomebus network")
  parser.add_argument("-n", dest="numnodes", default=1, type=int,
                      help="number of nodes to simulate")
  parser.add_argument("-i", dest="ifname", default="awesome", 
                      help="virtual network interface name")
  parser.add_argument("-q", dest="quiet", action="store_true", 
                      help="don't print anything")
  args = parser.parse_args()
  
  if not args.quiet:
    logging.basicConfig(level=logging.INFO, format="%(levelname)s: %(message)s")

  # create virtual ethernet interface
  f = os.open("/dev/net/tun", os.O_RDWR)
  ifformat = "%s%s" % (args.ifname, "%d")
  ifs = ioctl(f, TUNSETIFF, struct.pack("16sH", ifformat, IFF_TUN))
  ifname = ifs[:16].strip("\x00")
  # Equivalent of 'ifconfig <ifname> up'
  # os.system("ifconfig %s up" % ifname)
  # Using the Linux capabilities system, only the current process keeps any 
  # special capabilites (so os.system(), which creates a new process, loses
  # CAP_NET_ADMIN, which we need for ifconfig).
  s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
  ifr = ifreq()
  ifr.ifr_ifrn = ifname
  ioctl(s.fileno(), SIOCGIFFLAGS, ifr)
  ifr.ifr_flags |= IFF_UP
  ioctl(s.fileno(), SIOCSIFFLAGS, ifr)
  logging.info("Created interface %s. Starting %d virtual nodes." % (
               args.ifname, args.numnodes))

  # create virtual nodes
  nodes = [VirtualNode() for i in range(args.numnodes)]

  try:
    while 1:
      r = select([f],[],[])[0][0]
      if r == f:
        pkt = array.array("c", os.read(f, 1500))
        rpkt = process_frame(nodes, pkt)
        if rpkt:
          os.write(f, rpkt)
  except KeyboardInterrupt:
    logging.info("shutting down")

  os.close(f)


if __name__=="__main__":
  main(sys.argv[1:])
