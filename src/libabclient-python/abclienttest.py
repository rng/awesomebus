#!/usr/bin/env python
# vim:set ts=2 sw=2 sts=2 et: 

import abclient
import unittest
from subprocess import Popen
import time

MQNAME = "/awesomed"

class SimulatedAwesomebusTest(unittest.TestCase):
  def setUp(self):
    self.awesomesim = Popen(["../awesomesim/awesomesim", "-q", "-n", "4"])
    time.sleep(0.3)
    self.awesomed = Popen(["../awesomed/awesomed", "-s", "mq", "-i", "awesome0", "-f", MQNAME])
    time.sleep(0.2)

  def tearDown(self):
    self.awesomed.terminate()
    self.awesomesim.terminate()


class TestReadWrite(SimulatedAwesomebusTest):

  def test_dhcp(self):
    """ Test DHCP """
    bus = abclient.Bus(MQNAME)
    bus.dhcp()
    self.assertEqual(len(bus.nodes), 4)
    for i in range(4):
      self.assertTrue(i in bus.nodes)

  def test_badnode(self):
    """ Test bad node address """
    bus = abclient.Bus(MQNAME)
    bus.dhcp()
    self.assertEqual(len(bus.nodes), 4)

    node = abclient.Node(5)
    r = bus.transfer(node.write(1,1), node.read(1))
    self.assertEqual(len(r), 1)
    self.assertFalse(r[0].value, 1)

  def test_readwrite(self):
    """ Test basic node read/write """
    bus = abclient.Bus(MQNAME)
    bus.dhcp()
    self.assertEqual(len(bus.nodes), 4)

    for i in range(10):
        r = bus.transfer(
                bus.nodes[1].write(2, i),
                bus.nodes[2].write(4, 50-i),
                bus.nodes[1].read (2),
                bus.nodes[2].read (4),
            )
        self.assertEqual(len(r), 2)
        self.assertEqual(r[0].reg, 2)
        self.assertEqual(r[0].value, i)
        self.assertEqual(r[1].reg, 4)
        self.assertEqual(r[1].value, 50-i)


if __name__ == "__main__":
  unittest.main()
