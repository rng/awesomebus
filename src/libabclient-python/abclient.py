# vim:set ts=2 sw=2 sts=2 et: 

import struct
import posix_ipc
from collections import namedtuple

(AD_NONE,
 AD_WRITE,
 AD_READ,
 AD_VALUE,
 AD_NEWNODE,
) = range(5)

ADValue = namedtuple("ADValue", "node reg value")
ADNewNode = namedtuple("ADNewNode", "node addr")
  

class Node:
  def __init__(self, addr):
    self.addr = addr

  def write(self, reg, value):
    return struct.pack("IIII", AD_WRITE, self.addr, reg, value)

  def read(self, reg):
    return struct.pack("IIII", AD_READ, self.addr, reg, 0)


class Bus:
  def __init__(self, daemonqueue):
    self.mqc = posix_ipc.MessageQueue(daemonqueue+"_cmd.1", 0)
    self.mqr = posix_ipc.MessageQueue(daemonqueue+"_rsp.1", 0)
    self.nodes = {}

  def __del__(self):
    self.mqc.close()
    self.mqr.close()

  def dhcp(self):
    #print "Initial node address assignment"
    for i in range(32):
      new = False
      node = Node(i)
      for r in self.transfer(node.write(1,1), node.read(1)):
        if isinstance(r, ADNewNode):
          self.nodes[r.addr] = Node(r.addr)
          #print "assigned new node", r.addr
          new = True
        elif r.node not in self.nodes and r.value == 1:
          #print "allocate existing node", r.node
          self.nodes[r.node] = Node(r.node)
          new = True
      if not new:
        break
    #print "Done"

  def transfer(self, *cmds):
    self.mqc.send("".join(cmds))
    r, p = self.mqr.receive()
    assert p == 1, ("bad priority: %s" % p)
    assert len(r)%16 == 0, "bad response length"
    nr = len(r)/16
    resps =[]
    for i in range(nr):
      cmd = struct.unpack_from("IIII", r, i*16)
      if cmd[0] == AD_VALUE:
        resps.append(ADValue(*cmd[1:]))
      elif cmd[0] == AD_NEWNODE:
        resps.append(ADNewNode(*cmd[1:-1]))
    return resps


if __name__ == "__main__":
  import sys
  bus = Bus(sys.argv[1])
  bus.dhcp()

  for i in range(10):
      r = bus.transfer(
              bus.nodes[1].write(2, i),
              bus.nodes[2].write(4, 50-i),
              bus.nodes[1].read (2),
              bus.nodes[2].read (4),
          )
      print "iteration %d: %s" % (i,r)

