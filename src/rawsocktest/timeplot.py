#!/usr/bin/env python

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import matplotlib.path as path

fig = plt.figure()

######### HISTOGRAM
ax = fig.add_subplot(311)
# histogram our data with numpy
data = np.genfromtxt("times.csv",delimiter=",")
cycle = data[:,0]
loop = data[:,1]
n, bins = np.histogram(loop, 20)
# get the corners of the rectangles for the histogram
left = np.array(bins[:-1])
right = np.array(bins[1:])
bottom = np.zeros(len(left))
top = bottom + n
XY = np.array([[left,left,right,right], [bottom,top,top,bottom]]).T
# get the Path object
barpath = path.Path.make_compound_path_from_polys(XY)
# make a patch out of it
patch = patches.PathPatch(barpath, facecolor='blue', alpha=0.8)
ax.add_patch(patch)
# update the view limits
ax.set_xlim(left[0], right[-1])
ax.set_ylim(bottom.min(), top.max())
ax.set_xlabel("Looback time (us)")
ax.set_ylabel("Packet count")

######### RAW TIMES (loopback)
ax = fig.add_subplot(312)
ax.plot(loop, 'g')
ax.set_xlabel("packet #")
ax.set_ylabel("Loop time (us)")
ax.grid()

######### RAW TIMES (loopback/cycle)
ax = fig.add_subplot(313)
ax.plot(loop, 'g')
ax.plot(cycle, 'r')
ax.set_xlabel("packet #")
ax.set_ylabel("Loop & Cycle time (us)")
ax.grid()

plt.subplots_adjust(left=0.12, right=0.95, top=0.95, hspace=0.4)
plt.show()
