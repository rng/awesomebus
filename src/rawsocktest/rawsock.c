#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <arpa/inet.h>
#include <netpacket/packet.h>
#include <sys/ioctl.h>
#include <linux/if.h>
#include <time.h>
#include <assert.h>
#include <sched.h>
#include "awesomebus.h"

#define CYCLE_TIME (1000) // time in us for one cycle (1000us = 1KHz)
#define PACKET_SIZE (100) // packet size in words

static const MacAddress broadcast_addr = {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF};

typedef struct {
	uint32_t cycle;
	uint32_t loopback;
} Timing;


static int create_socket(char *interface, struct sockaddr_ll *addr) {
	struct ifreq ifr;

	// open socket
	int s = socket(PF_PACKET, SOCK_RAW, htons(AB_PROTO));
	if (s < 0) {
		perror("socket() error");
		exit(-1);
	}
	// get interface number
	memset(&ifr, 0, sizeof(ifr));
	strcpy(ifr.ifr_name, interface);
	if (ioctl(s, SIOCGIFINDEX, &ifr) == -1) {
		perror("can't get if interface");
		exit(-1);
	}
	// create address structure
	memset(addr, 0, sizeof(addr));
	addr->sll_family = AF_PACKET;
	addr->sll_protocol = htons(AB_PROTO);
	addr->sll_halen = 6;
	addr->sll_ifindex = ifr.ifr_ifindex;
	memcpy(&(addr->sll_addr), broadcast_addr, 6);
	// bind to socket
	if (bind(s, (struct sockaddr*)addr, sizeof(struct sockaddr_ll)) < 0) {
		perror("bind failed");
		exit(-1);
	}
	// return final socket
	return s;
}


static void loopback(int s, struct sockaddr_ll *addr, uint8_t *pkt, uint8_t *rpkt, int len) {
	socklen_t addrlen = sizeof(struct sockaddr_ll);
	// send request
	if (sendto(s, pkt, len, 0, (struct sockaddr*)addr, addrlen) < 0) {
		perror("sendto failed");
		exit(-1);
	}
	// read response
	if (recvfrom(s, rpkt, len, 0, (struct sockaddr*)addr, &addrlen) < 0) {
		perror("recvfrom failed");
		exit(-1);
	}
}


static struct timespec diff(struct timespec start, struct timespec end) {
	struct timespec temp;
	if ((end.tv_nsec-start.tv_nsec)<0) {
		temp.tv_sec = end.tv_sec-start.tv_sec-1;
		temp.tv_nsec = 1000000000+end.tv_nsec-start.tv_nsec;
	} else {
		temp.tv_sec = end.tv_sec-start.tv_sec;
		temp.tv_nsec = end.tv_nsec-start.tv_nsec;
	}
	return temp;
}


int main(int argc, char *argv[]) {
	Timing *times;

	if (argc!=3) {
		printf("usage: ./rawsock <interface> <packet count>\n");
		exit(1);
	}
	int count = atoi(argv[2]);

	// use FIFO scheduler with high priority
	struct sched_param sp;
	if (sched_getscheduler(0) == -1) {
		perror("couldn't get scheduler");
		exit(-1);
	}
	sp.sched_priority = sched_get_priority_max(SCHED_FIFO);
	sched_setscheduler(0, SCHED_FIFO, &sp);

	// create socket
	struct sockaddr_ll addr;
	int s = create_socket(argv[1], &addr);

	struct timespec t0, t1, tlast;
	times = (Timing*)malloc(sizeof(Timing) * count);

	// create packet
	uint8_t pkt[1024];
	uint8_t rpkt[1024];
	uint32_t len;
	memset(pkt, 0, sizeof(pkt));
	memset(rpkt, 0, sizeof(rpkt));
	ABFrame *frame = (ABFrame*)pkt;
	//ABFrame *rframe = (ABFrame*)rpkt;
	uint32_t buckets[128];
	ab_frame_create(frame);
		AB_ADD_BUCKET(frame, 1, 
				AB_READ(0), 
				AB_READ(1), 
				AB_READ(2),
				AB_READ(3),
				AB_WRITE(0, 0x123456),
				AB_WRITE(1, 0x202020),
				AB_WRITE(2, 0x303030),
				AB_WRITE(3, 0x404040),
		);
	for (int i=2;i<=32;i++) {
		AB_ADD_BUCKET(frame, i, 
				AB_READ(0), 
				AB_READ(1), 
				AB_READ(2), 
				AB_READ(3)
		);
	}
	ab_frame_finish(frame, &len, buckets);

	ab_frame_dump(frame, buckets);

	// test loopback
	printf("sending %d x %db packets\n", count, len);
	clock_gettime(CLOCK_REALTIME, &tlast);
	for (int i=0; i<count; i++) {
		// run loopback
		clock_gettime(CLOCK_REALTIME, &t0);
		loopback(s, &addr, pkt, rpkt, len);
		clock_gettime(CLOCK_REALTIME, &t1);
		//ab_frame_decode(frame, rframe, buckets);
		// calculate timing
		struct timespec tdiff = diff(t0, t1);
		assert((long)tdiff.tv_sec == 0);
		// try to keep cycle time correct
		if ((tdiff.tv_nsec / 1000) < CYCLE_TIME) {
			usleep(CYCLE_TIME - (tdiff.tv_nsec/1000));
		}
		times[i].loopback = tdiff.tv_nsec/1000;
		// save timing
		struct timespec tcycle = diff(tlast, t0);
		tlast = t0;
		times[i].cycle = tcycle.tv_nsec/1000;
	}
	close(s);

	// save timings to file
	FILE *log;
	uint64_t tavg=0, tmin=100000000, tmax=0;
	log = fopen("times.csv", "wb");
	for (int i=0; i<count; i++) {
		uint64_t tloop = times[i].loopback;
		tavg += tloop;
		tmin = tloop < tmin ? tloop : tmin;
		tmax = tloop > tmax ? tloop : tmax;
		fprintf(log, "%d,%d\n", times[i].cycle, times[i].loopback);
	}
	fclose(log);
	
	// print stats
	tavg /= count;
	printf("tloop avg: %dus min: %dus max: %dus\n", 
					(int)(tavg), (int)(tmin), (int)(tmax)
				);

	free(times);
	return 0;
}
