#!/usr/bin/env python

lines = [[y[1:-1] for y in x.split(",")] for x in file("pkt.csv").readlines()]
tlast = 0

print " #   t_req        t_resp  | t_delta t_loop | ty_req ty_resp"
print "--------------------------+----------------+---------------"

i = 1
while i < len(lines):
	# request
	l = lines[i]
	i += 1
	an = int(l[0])
	at = float(l[1])
	ap = int(l[4],16)
	if ap != 0x0801:
		raise Exception("request has wrong ethertype")

	# response
	l = lines[i]
	i += 1
	bn = int(l[0])
	bt = float(l[1])
	bp = int(l[4],16)
	if bp <= 0x0900:
		raise Exception("response has wrong ethertype")

	# print
	tdelta = at-tlast
	print "%3d %6.2fms <-> %6.2fms | %4.0fus %4.0fus  | %04x   %04x    " % (
				bn, at*1000, bt*1000, tdelta*1000000, (bt-at)*1000000, ap, bp)
	tlast = at
