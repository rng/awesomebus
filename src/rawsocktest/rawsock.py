#!/usr/bin/env python

import sys
import socket
import time
import struct

if len(sys.argv) != 3:
	print "usage: ./rawsock.py <interface> <packet count>"
	sys.exit(1)

interface = sys.argv[1]
count = int(sys.argv[2])
payloadsize = 100

s = socket.socket(socket.AF_PACKET, socket.SOCK_RAW, 0x8888)
s.bind((interface, 0))

# build packet
broadcast_addr = (0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF)
src_addr = struct.pack("!BBBBBB", *broadcast_addr)
dst_addr = struct.pack("!BBBBBB", *broadcast_addr)
typesize= struct.pack("!HH", 0x8888, payloadsize)
playload = "".join([(chr((((i)%16)<<4)|(i)%16))*4 for i in range(payloadsize)])
pkt = dst_addr+src_addr+typesize+playload
# XMOS code doesn't do non-word-lenght packets
assert len(pkt)%4 == 0

# send
print "sending %d packets of %dbytes" % (count, len(pkt))
dtotal = 0

tavg = 0
tmax = 0
tmin = 100000000

tstart = t0 = time.time()
for i in range(count):
	tsend = time.time()
	s.send(pkt)
	d = s.recvfrom(0xffff)
	trecv = time.time()
	dtotal += len(d[0])
	tloop = (trecv-tsend)
	tavg += tloop
	tmax = max(tmax, tloop)
	tmin = min(tmin, tloop)
	while time.time()-t0 < 0.001:
		pass
	t0 = time.time()


tavg /= count
print "received: %d bytes over %5.2fs" % (dtotal, time.time()-tstart)
print "tloop avg: %5.1fus min: %5.1fus max: %5.1fus" % (tavg*1000000, tmin*1000000, tmax*1000000)
