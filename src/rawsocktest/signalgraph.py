#!/usr/bin/env python

import Image, ImageFont, ImageDraw
import math

KHz = 1000
MHz = 1000*1000
B = 8
KB = 1000*B
ns = 1
us = 1000
ms = 1000*1000

def prettytime(t):
    if t==0:
        return "0"
    elif t<=1000:
        return "%dns" % t
    elif t<=1000000:
        return "%0.1fus" % (t/us)
    else:
        return "%0.1fms" % (t/ms)

class Signal:
    def __init__(self, **opts):
        self.__dict__.update(opts)
        self.times = 1
        self.period = 0

    def at(self, t):
        self.t = t
        return self

    def at_start(self, sig, t=0):
        self.t = sig.t_start() + t
        return self

    def at_end(self, sig, t=0):
        self.t = sig.t_end() + t
        return self

    def repeat(self, times, period):
        self.times = times
        self.period = period
        return self

    def regions(self):
        r = []
        for i in range(self.times):
            t0 = self.t_start() + i*self.period
            t1 = t0 + self.duration()
            r.append((t0,t1))
        return r

    def duration(self):
        return int(self.length/float(self.clock)*1000*1000*1000)

    def t_start(self):
        return self.t

    def t_end(self):
        return self.t + self.duration()


CYCLES = 2

# 100 Mbit ethernet, 1KB packet
eth_in =  (Signal(clock=100*MHz, length=1*KB)
           .at(0*ns)
           .repeat(CYCLES, 1*ms))
# Node passthrough delay of 5us
eth_out = (Signal(clock=100*MHz, length=1*KB)
           .at_start(eth_in, 5*us)
           .repeat(CYCLES, 1*ms))
# Node processing completes after 100us, triggers ARM interrupt
intr =    (Signal(clock=10*MHz, length=1)
           .at_start(eth_in, 100*us)
           .repeat(CYCLES, 1*ms))
# XMOS->ARM reads via SPI at 5MHz, 8 regs (4*8 + some)
spi_rd =  (Signal(clock=5*MHz, length=4*10*B)
           .at_start(intr)
           .repeat(CYCLES, 1*ms))
# ARM processing 10K instructions at 50MHz?
spi_t   = (Signal(clock=50*MHz, length=10000)
           .at_end(spi_rd)
           .repeat(CYCLES, 1*ms))
# ARM->XMOS writes via SPI, 8 regs
spi_wr =  (Signal(clock=5*MHz, length=4*10*B)
           .at_end(spi_t)
           .repeat(CYCLES, 1*ms))

signals = []
for gn in sorted(globals().keys()):
    g = globals()[gn]
    if isinstance(g, Signal):
        signals.append((gn, g))

WIDTH = 500
HEIGHT = len(signals)*10 + 20
image = Image.new("RGB", (WIDTH,HEIGHT), (256,256,256))
draw = ImageDraw.Draw(image)
font = ImageFont.truetype("/usr/share/fonts/truetype/ttf-dejavu/DejaVuSans.ttf", 9)

# calulate various sizes
vticks = 8
lblwidth = max([font.getsize(lbl)[0] for lbl, s in signals])+10
sigwidth = WIDTH-lblwidth-30
sigtime = max([max([r[1] for r in s.regions()]) for l, s in signals])
rounddiv = vticks * (10**(int(math.log(sigtime, 10))-1))
sigtime = math.ceil(sigtime/float(rounddiv))*rounddiv
scale = sigwidth / float(sigtime)

# draw horizontal lines
for i in range(1,len(signals)+2):
    draw.line((0,i*10,WIDTH,i*10), fill=(200,200,200))

# draw vertical lines
for i in range(vticks+5):
    x = lblwidth + (sigwidth/float(vticks)*i)
    t = sigtime/float(vticks)*i
    draw.line((x, 0, x, HEIGHT), fill=(200,200,200))
    draw.text((x,0), prettytime(t), font=font, fill=(0,0,0))

# draw signals
for i, sig in enumerate(signals):
    lbl, s = sig
    y = i*10 + 10
    draw.text((2,y), lbl, font=font, fill=(0,0,0))
    for r in s.regions():
        x0 = r[0]*scale + lblwidth
        x1 = r[1]*scale + lblwidth
        draw.rectangle((x0, y+2, x1, y+9), fill=(100,100,100))

image.save("signals.png")

