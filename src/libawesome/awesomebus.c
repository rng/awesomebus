/* vim:set ts=2 sw=2 sts=2 et: */
#include <string.h>
#include <arpa/inet.h>
#include <netpacket/packet.h>
#include <linux/if.h>
#include <sys/ioctl.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include "awesomebus.h"

static const MacAddress broadcast_addr = {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF};
static char errorbuf[128];

#define ETHERNET_MIN_BYTES (64)


// Create a new awesomebus interface
ABError ab_interface_open(ABInterface *intf, char *ethif) {
  struct ifreq ifr;

  // open socket
  intf->socket = socket(PF_PACKET, SOCK_RAW, htons(AB_PROTO));
  if (intf->socket < 0) {
    perror("socket() error");
    return AB_ERR_GENERIC;
  }
  // get interface number
  memset(&ifr, 0, sizeof(ifr));
  strcpy(ifr.ifr_name, ethif);
  if (ioctl(intf->socket, SIOCGIFINDEX, &ifr) == -1) {
    perror("can't get if interface");
    return AB_ERR_GENERIC;
  }
	struct sockaddr_ll *addr = &(intf->addr);
  // create address structure
  memset(addr, 0, sizeof(addr));
  addr->sll_family = AF_PACKET;
  addr->sll_protocol = htons(AB_PROTO);
  addr->sll_halen = 6;
  addr->sll_ifindex = ifr.ifr_ifindex;
  memcpy(&(addr->sll_addr), broadcast_addr, 6);
  // bind to socket
  if (bind(intf->socket, (struct sockaddr*)addr, sizeof(struct sockaddr_ll)) < 0) {
    perror("bind failed");
    return AB_ERR_GENERIC;
  }
  // set receive timeout
  struct timeval tv;
  tv.tv_sec = 0;
  tv.tv_usec = 100000;
  setsockopt(intf->socket, SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof(struct timeval));
  // return final socket
  return AB_ERR_OK;
}


// Close an awesomebus interface
ABError ab_interface_close(ABInterface *intf) {
  if (intf->socket) {
    close(intf->socket);
  }
  return AB_ERR_OK;
}


// return a descriptive string for an errorcode
char *ab_error_string(ABError errcode) {
  memset(errorbuf, 0, 128);
  switch (errcode) {
    case AB_ERR_GENERIC: 
      strncpy(errorbuf, "generic error", 127); 
      break;
    case AB_ERR_TIMEOUT: 
      strncpy(errorbuf, "timeout", 127); 
      break;
    default:
      sprintf(errorbuf, "unknown error %d", errcode);
      break;
  }
  return errorbuf;
}


// process a packet through the awesomebus interface, and return resulting packet
ABError ab_interface_process(ABInterface *intf, uint8_t *pkt, uint8_t *rpkt, int len) {
  socklen_t addrlen = sizeof(struct sockaddr_ll);
  struct sockaddr *addr = (struct sockaddr*)&intf->addr;
  // send request
  if (sendto(intf->socket, pkt, len, 0, addr, addrlen) < 0) {
    perror("sendto failed");
    return AB_ERR_GENERIC;
  }
  // read response
  if (recvfrom(intf->socket, rpkt, len, 0, addr, &addrlen) < 0) {
    if (errno == EAGAIN) {
      return AB_ERR_TIMEOUT;
    }
    perror("recvfrom failed");
    return AB_ERR_GENERIC;
  }
  return AB_ERR_OK;
}


// Return the frame bytesize of a frame
static uint32_t ab_frame_bytesize(ABFrame *frame) {
  return 16 + (frame->wordcount * 4);
}


// Initialise an awesomebus ethernet frame
void ab_frame_create(ABFrame *frame) {
  memcpy(frame->dest, broadcast_addr, 6);
  memcpy(frame->src, broadcast_addr, 6);
  frame->ethertype = htons(AB_PROTO);
  frame->wordcount = 0;
}


// Finalise the frame (reorder for network). Don't modify the frame after this
void ab_frame_finish(ABFrame *frame, uint32_t *len, uint32_t *buckets) {
  // If the frame is less than the Ethernet minimum, pad it
  uint32_t framelen = ab_frame_bytesize(frame);
  if (framelen < ETHERNET_MIN_BYTES) {
    uint32_t paddingbytes = ETHERNET_MIN_BYTES - framelen;
    uint32_t inst[ETHERNET_MIN_BYTES] = {0};
    // make a bucket for a (hopefully) non-existant node
    ab_frame_add_bucket(frame, 0xFE, paddingbytes/4, inst); \
  }
  *len = ab_frame_bytesize(frame);

  uint32_t i = 0, bktcount = 0; 
  while (i<frame->wordcount) {
    uint32_t offset = 16 + (i*4);
    ABBucket *bkt = (ABBucket*)(((char*)frame) + offset);
    buckets[bktcount++] = offset;
    //printf("reordering bucket @ %d : %d %d\n", i, bkt->node, bkt->length);
    for (int j=0;j<bkt->length;j++) {
      bkt->instructions[j] = htonl(bkt->instructions[j]);
    }
    i += (bkt->length + 1);
    bkt->length = htons(bkt->length);
    bkt->node = htons(bkt->node);
  }
  frame->wordcount = htons(frame->wordcount);
}


// Add a bucket to the frame
void ab_frame_add_bucket(ABFrame *frame, uint16_t n, uint16_t len, uint32_t *inst) {
  ABBucket *bkt = (ABBucket*)(((char*)frame) + ab_frame_bytesize(frame));
  bkt->node = n;
  bkt->length = len;
  memcpy(bkt->instructions, inst, 4*len);
  frame->wordcount += (1 + len);
}


// Decode the contents of a returned frame
void ab_frame_decode(ABFrame *frame, ABFrame *rframe, uint32_t *buckets, int verbose) {
  if (verbose) {
    printf("Decoding frame with %d words:\n", ntohs(frame->wordcount));
  }
  rframe->wordcount = ntohs(frame->wordcount);
  uint32_t wordpos = 0;
  int i = 0;
  while (wordpos < ntohs(frame->wordcount)) {
    ABBucket *bkt = (ABBucket*)(((char*)frame) + buckets[i]);
    ABBucket *rbkt = (ABBucket*)(((char*)rframe) + buckets[i++]);
    ab_bucket_decode(bkt, rbkt, verbose);
    wordpos += (1 + ntohs(bkt->length));
  }
}


// Dump the contents of a frame
void ab_frame_dump(ABFrame *frame, uint32_t *buckets) {
  printf("Dumping frame with %d words:\n", ntohs(frame->wordcount));
  uint32_t wordpos = 0;
  int i = 0;
  while (wordpos < ntohs(frame->wordcount)) {
    ABBucket *bkt = (ABBucket*)(((char*)frame) + buckets[i++]);
    ab_bucket_dump(bkt);
    wordpos += (1 + ntohs(bkt->length));
  }
  printf("\n");
}


// Decode the contents of a bucket
void ab_bucket_decode(ABBucket *bkt, ABBucket *rbkt, int verbose) {
  uint16_t node = ntohs(bkt->node);
  uint16_t length = ntohs(bkt->length);
  if (verbose) {
    printf(" Bucket for node 0x%04x, %d words:\n", node, length);
  }
  rbkt->node = node;
  rbkt->length = length;
  for (int i = 0; i < (length); i++) {
    uint32_t inst = ntohl(bkt->instructions[i]);
    rbkt->instructions[i] = ntohl(rbkt->instructions[i]); //inst;
    //rbkt->instructions[i] = (rbkt->instructions[i] << 1) + ((inst>>4)&0xF);
    if (verbose) {
      uint8_t op = inst&0xF;
      switch (op) {
      case AB_OP_READ:
        printf("  read  r%02d = 0x%06x\n", (inst>>4)&0xF, rbkt->instructions[i]>>8);
        break;
      }
    }
  }
}


// dump the contents of a bucket
void ab_bucket_dump(ABBucket *bkt) {
  uint16_t node = ntohs(bkt->node);
  uint16_t length = ntohs(bkt->length);
  printf(" Bucket for node 0x%04x, %d words:\n", node, length);
  for (int i=0; i<length/4; i++) {
    uint32_t inst = ntohl(bkt->instructions[i]);
    uint8_t op = inst&0xF;
    printf("  %02x: %08x  ", i , inst);
    switch (op) {
    case AB_OP_READ:
      printf("read  r%02d\n", (inst>>4)&0xF);
      break;
    case AB_OP_WRITE:
      printf("write r%02d, 0x%06x\n", (inst>>4)&0xF, inst>>8);
      break;
    case AB_OP_SETADDR:
      printf("dhcp   %d\n", (inst>>8));
      break;
    default:
      printf("-\n");
      break;
    }
  }
}

