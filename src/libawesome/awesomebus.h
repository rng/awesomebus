/* vim:set ts=2 sw=2 sts=2 et: */
#ifndef _AWESOMEBUS_H_
#define _AWESOMEBUS_H_

#include <stdint.h>
#include <netpacket/packet.h>

// our ethertype
#define AB_PROTO (0x8888)

// node broadcast address
#define AB_BROADCAST_ADDR (0xFF)

// Bucket Ops
#define AB_OP_NOP     0x0
#define AB_OP_READ    0x1
#define AB_OP_WRITE   0x2
#define AB_OP_PING    0x3
#define AB_OP_SETADDR 0x4
#define AB_OP_CONFIG  0x5

// Bucket Commands
#define AB_NOP               ((AB_OP_NOP))
#define AB_READ(reg)         ((AB_OP_READ)|(reg<<4))
#define AB_WRITE(reg, value) ((AB_OP_WRITE)|(reg<<4)|(value<<8))
#define AB_PING()            ((AB_OP_PING))
#define AB_SETADDR(value)    ((AB_OP_SETADDR)|(value<<8))
#define AB_CONFIGREAD(addr)  ((AB_OP_CONFIG)|(1<<4)|(addr<<5))
#define AB_CONFIGWRITE(addr, value) ((AB_OP_CONFIG)|(0<<4)|(addr<<5)|(value<<16))

#define AB_CODESIZE(code) (sizeof(code)/4)

#define AB_ADD_BUCKET(frame, n, ...) { \
  uint32_t _inst[] = {__VA_ARGS__}; \
  ab_frame_add_bucket(frame, n, AB_CODESIZE(_inst), _inst); \
}

// Error codes
typedef enum {
  AB_ERR_OK      = 0,
  AB_ERR_GENERIC = -1,
  AB_ERR_TIMEOUT = -2,
} ABError;

typedef unsigned char MacAddress[6];

typedef struct {
  int socket;
  struct sockaddr_ll addr;
} ABInterface;

typedef struct __attribute__((__packed__)) {
  uint16_t node;
  uint16_t length;
  uint32_t instructions[];
} ABBucket;

typedef struct __attribute__((__packed__)){
  MacAddress dest;
  MacAddress src;
  uint16_t ethertype;
  uint16_t wordcount;
} ABFrame;

// Interface
ABError ab_interface_open(ABInterface *intf, char *ethif);
ABError ab_interface_close(ABInterface *intf);
ABError ab_interface_process(ABInterface *intf, uint8_t *pkt, uint8_t *rpkt, int len);
char *ab_error_string(ABError errcode);

// Frame
void ab_frame_create(ABFrame *frame);
void ab_frame_finish(ABFrame *frame, uint32_t *len, uint32_t *buckets);
void ab_frame_add_bucket(ABFrame *frame, uint16_t n, uint16_t len, uint32_t *inst);
void ab_frame_decode(ABFrame *frame, ABFrame *rframe, uint32_t *buckets, int verbose);
void ab_frame_dump(ABFrame *frame, uint32_t *buckets);

// Bucket
void ab_bucket_decode(ABBucket *bkt, ABBucket *rbkt, int verbose);
void ab_bucket_dump(ABBucket *bkt);

#endif
