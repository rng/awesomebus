#!/usr/bin/env python
import abclient
import time
import math

class LowPassFilter:
	def __init__(self, alpha, freq):
		self.alpha = alpha
		self.freq = freq
		self.vEstimate = 0

	def update(self, vMeasured):
		self.vEstimate = ((self.alpha * self.vEstimate) + 
											((1-self.alpha) * vMeasured * self.freq))
		return self.vEstimate

FREQ = 1000

lpfilter = LowPassFilter(0.99, FREQ) #0)

b = abclient.Bus("/awesomed")
n = abclient.Node(1)

CMAX = 4000 #2048
v = 0
i = 0
#tw = 0.0009
tw = 1/float(FREQ)
drivemax = 3.5

Kp = 150
Ki = 1.00

try:
  
	start = start0 = time.time()
	iterations = 0

	pos = 0
	opos = 0

	ierr = 0
	cur = 0
	temp = 0
	count = 0
	estvel = 0

	avgcur = []
	avgtemp = []
	avgcount = 20
	ts = 0

	f = file("out.csv","wb")
	f.write('"time","position","avgcurrent","avgtemp","count","targvel","estvel","pwm","dterror"\n')

	while 1:
		start = time.time()

		# PID
		vel = pos - opos
		if vel < 0:
			vel = 33536 + vel
		if vel < 20:
			estvel = lpfilter.update(vel)/float(FREQ)
		# drive motor with sine
		velt = ((drivemax/2.0)+(math.sin(-math.pi/2 + (2*math.pi)*i/float(CMAX-1))*(drivemax/2.0)))
		
		verr = velt - estvel
		#verr = estvel - velt
		ierr += verr

		drive = (verr*Kp) + (ierr*Ki)
		if drive < 0:
			drive = 0
		elif drive > 255:
			drive = 255
		
		#drive = velt
		
		#sys.stdout.write("\rvel:%0.2f, velt:%0.2f, verr:%7.2f, ierr:%7.2f, cur:%4d, drive:%5d   " % (estvel, velt, verr, ierr, cur, drive))
		#sys.stdout.flush()

		#sys.stdout.write("\rpos:%5d cur:%5d temp:%5d count:%5d" % (pos, cur, temp, count))
		#sys.stdout.flush()
		#if len(avgcur) > avgcount:
		#	avgcur.pop(0)
		#avgcur.append(cur)
		#if len(avgtemp) > avgtemp:
		#	avgtemp.pop(0)
		#avgtemp.append(temp)

		#acur = sum(avgcur) / float(len(avgcur))
		#atemp = sum(avgtemp) / float(len(avgtemp))
		#atemp = (3.3*(atemp/1024.0) - 0.5) * 100
		acur = 0
		atemp = 0
		if iterations > 0:
			f.write("%f,%d,%d,%0.1f,%d,%0.2f,%0.2f,%d,%0.5f\n" % (
							time.time()-start0, pos, acur, atemp, 
							count, velt, estvel, drive, 1 - (ts*1000)))

		r = b.transfer(
					n.write(0,int(drive)), 
					n.read(1), 
					n.read(2), 
					n.read(3), 
					n.read(4)
				)
		i = (i+1)%CMAX
		
		if isinstance(r[0], abclient.ADValue):
			opos = pos
			pos = r[0].value
		if len(r) > 1 and isinstance(r[1], abclient.ADValue):
			cur = r[1].value
		if len(r) > 2 and isinstance(r[2], abclient.ADValue):
			temp = r[2].value
		if len(r) > 3 and isinstance(r[3], abclient.ADValue):
			count = r[3].value

		ts = tw-(time.time()-start)
		if ts>0:
			time.sleep(ts)
		iterations += 1
		if iterations == 2*CMAX:
			break

except KeyboardInterrupt:
	pass

print
f.close()
b.transfer(n.write(0,0))
