/* vim:set ts=2 sw=2 sts=2 et: */
#include <abclient.h>
#include <stdio.h>
#include <stdint.h>
#include <math.h>
#include <unistd.h>
#include <time.h>
#include <assert.h>
#include <sched.h>
#include <stdlib.h>

#define CMAX (4000)
#define DRIVEMAX (3.5)
#define PI (3.14159)
#define CYCLE_TIME (1000) // time in us for one cycle (1000us = 1KHz)
#define FREQ (1000.0)

#define Kp (400.0)
#define Ki (2.0)

static struct timespec diff(struct timespec start, struct timespec end) {
	struct timespec temp;
	if ((end.tv_nsec-start.tv_nsec)<0) {
		temp.tv_sec = end.tv_sec-start.tv_sec-1;
		temp.tv_nsec = 1000000000+end.tv_nsec-start.tv_nsec;
	} else {
		temp.tv_sec = end.tv_sec-start.tv_sec;
		temp.tv_nsec = end.tv_nsec-start.tv_nsec;
	}
	return temp;
}

typedef struct {
  float alpha;
  float freq;
  float vestimate;
} lpfilter;

float lpfilter_update(lpfilter *f, float vMeasured) {
  f->vestimate = ((f->alpha * f->vestimate) +
                  ((1 - f->alpha) * vMeasured * f->freq));
  return f->vestimate/f->freq;
}

int main(int argc, char *argv[]) {
  ABClient abc;
  int32_t r_pos = 0, r_cur, r_temp, r_count;
  FILE *logfile;
  float time = 0;
  float targvel = 0;
  float estvel = 0;
  float dterror = 0;
  int pwm = 0;
  int32_t vel, opos = 0;
	struct timespec t0, t1, t2;
  float ierr = 0;
  lpfilter lpf = { 0.99, FREQ, 0 };

	// use FIFO scheduler with high priority
	struct sched_param sp;
	if (sched_getscheduler(0) == -1) {
		perror("couldn't get scheduler");
		exit(-1);
	}
	sp.sched_priority = sched_get_priority_max(SCHED_FIFO);
	sched_setscheduler(0, SCHED_FIFO, &sp);

  abclient_open(&abc, "/awesomed");
  logfile = fopen("out.csv","wb");
  fprintf(logfile, "\"time\",\"position\",\"avgcurrent\",\"avgtemp\",\"count\",\"targvel\",\"estvel\",\"pwm\",\"dterror\"\n");

  clock_gettime(CLOCK_REALTIME, &t0);

  for (int iter=0;iter<2*CMAX;iter++) {

    vel = r_pos - opos;
    if (vel < 0)
      vel = 33536 + vel;
    if (vel < 20)
      estvel = lpfilter_update(&lpf, vel);
    
    targvel = (DRIVEMAX/2.0)+(sin(-(PI/2.0)+(2*PI)*iter/(float)(CMAX-1))*(DRIVEMAX/2.0));
    pwm = 0;

    float verr = targvel - estvel;
    ierr += verr;

    pwm = (int)( (verr*Kp) + (ierr*Ki) );
    if (pwm < 0)
      pwm = 0;
    if (pwm > 255)
      pwm = 255;

    opos = r_pos;

		clock_gettime(CLOCK_REALTIME, &t1);
		//ab_frame_decode(frame, rframe, buckets);
		// calculate timing
		struct timespec tdiff = diff(t0, t1);
    time = (tdiff.tv_sec + ((float)(tdiff.tv_nsec)/1000000000.0));

    // setup regs
    abclient_write(&abc, 1, 0, pwm);
    abclient_read(&abc, 1, 1);
    abclient_read(&abc, 1, 2);
    abclient_read(&abc, 1, 3);
    abclient_read(&abc, 1, 4);
    // transfer
    int r = abclient_transfer(&abc);
    // parse response
    for (int i=0;i<r;i++) {
      if (abc.packets[i].cmd == AD_VALUE) {
        uint32_t value = abc.packets[i].data.v.value;
        switch (abc.packets[i].data.v.reg) {
          case 1: r_pos = value; break;
          case 2: r_cur = value; break;
          case 3: r_temp = value; break;
          case 4: r_count = value; break;
        }
      }
    }
    // log
    fprintf(logfile,"%f,%d,%d,%d,%d,%f,%f,%d,%f\n",
            time, r_pos, r_cur, r_temp, r_count, targvel, 
            estvel, pwm, dterror);
		
    clock_gettime(CLOCK_REALTIME, &t2);
		// calculate timing
		struct timespec tdiff2 = diff(t1, t2);
		assert((long)tdiff2.tv_sec == 0);
		// try to keep cycle time correct
		if ((tdiff2.tv_nsec / 1000) < CYCLE_TIME) {
			usleep(CYCLE_TIME - (tdiff2.tv_nsec/1000));
		}

    clock_gettime(CLOCK_REALTIME, &t2);
		// calculate timing
		tdiff2 = diff(t1, t2);
		assert((long)tdiff2.tv_sec == 0);
    dterror = (tdiff2.tv_nsec/1000000.0) - 1.0;
  }

  fclose(logfile);

  abclient_close(&abc);
  return 0;
}
