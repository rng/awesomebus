#!/usr/bin/env python
from pylab import plotfile, show
import sys

plotfile(sys.argv[1], [0]+map(int, sys.argv[2:]))
show()
