#ifndef _ABCLIENT_H_
#define _ABCLIENT_H_

#include <mqueue.h>
#include <stdint.h>

#define MAX_PACKETS (64)

#define AD_WRITE   (1)
#define AD_READ    (2)
#define AD_VALUE   (3)
#define AD_NEWNODE (4)

// Awesomed Packet structure - tagged union
typedef struct {
  uint32_t cmd;
  uint32_t node;
  union {
    // write
    struct { 
      uint32_t reg;
      uint32_t value;
    } w;
    // read
    struct {
      uint32_t reg;
    } r;
    // value
    struct {
      uint32_t reg;
      uint32_t value;
    } v;
    // new node
    struct {
      uint32_t addr;
    } n;
  } data;
} ADPacket;


typedef struct {
  mqd_t mqc;
  mqd_t mqr;
  ADPacket packets[MAX_PACKETS];
  int npackets;
} ABClient;

int abclient_open(ABClient* abc, char *mq_name);
int abclient_close(ABClient *abc);
int abclient_transfer(ABClient *abc);
int abclient_read(ABClient *abc, int node, int reg);
int abclient_write(ABClient *abc, int node, int reg, int value);

#endif
