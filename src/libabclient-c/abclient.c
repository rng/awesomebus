/* vim:set ts=2 sw=2 sts=2 et: */
#include <stdio.h>
#include "abclient.h"

#define MQ_MODE S_IRUSR|S_IWUSR 
#define MQMSG_SIZE (256)

int abclient_open(ABClient* abc, char *mq_name) {
  struct mq_attr attr;
  char cmdq_name[64];
  char rspq_name[64];

  // set mq attributes
  attr.mq_maxmsg = 8;
  attr.mq_msgsize = MQMSG_SIZE;
  attr.mq_flags = 0;
  attr.mq_curmsgs = 0;

  // build queue names
  snprintf(cmdq_name, 64, "%s_cmd.1", mq_name);
  snprintf(rspq_name, 64, "%s_rsp.1", mq_name);
  
  // open mq
  if ((abc->mqc = mq_open(cmdq_name, O_RDWR|O_CREAT, MQ_MODE, &attr)) < 0) {
    printf("Couldn't open command message queue\n");
    perror("stuff");
    return -1;
  }
  if ((abc->mqr = mq_open(rspq_name, O_RDWR|O_CREAT, MQ_MODE, &attr)) < 0) {
    printf("Couldn't open response message queue\n");
    perror("stuff");
    return -1;
  }
  abc->npackets = 0;

  return 0;
}


int abclient_close(ABClient *abc) {
  mq_close(abc->mqc);
  mq_close(abc->mqr);
  return 0;
}


int abclient_transfer(ABClient *abc) {
  int datalen = abc->npackets * sizeof(ADPacket);
  int recvlen = 0;

  // send command packets to queue
  if (mq_send(abc->mqc, (char*)abc->packets, datalen, 0) == -1) {
    perror("mq_send");
    return -1;
  }

  // read response packets from queue
  int bufsize = MAX_PACKETS * sizeof(ADPacket);
  uint32_t priority;

  recvlen = mq_receive(abc->mqr, (char*)abc->packets, bufsize, &priority);
  if (recvlen == -1) {
    perror("mq_receive");
    return -1;
  }

  // check received packet length
  if ((recvlen % sizeof(ADPacket)) != 0) {
    printf("invalid receive len: %d\n", recvlen);
    return -1;
  }

  abc->npackets = 0;

  return recvlen/sizeof(ADPacket);
}


int abclient_read(ABClient *abc, int node, int reg) {
  if (abc->npackets == MAX_PACKETS)
    return -1;
  ADPacket *pkt = &(abc->packets[abc->npackets++]);
  pkt->cmd = AD_READ;
  pkt->node = node;
  pkt->data.r.reg = reg;
  return 0;
}


int abclient_write(ABClient *abc, int node, int reg, int value) {
  if (abc->npackets == MAX_PACKETS)
    return -1;
  ADPacket *pkt = &(abc->packets[abc->npackets++]);
  pkt->cmd = AD_WRITE;
  pkt->node = node;
  pkt->data.w.reg = reg;
  pkt->data.w.value = value;
  return 0;
}
